<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('location');
            $table->string('photograph');
            $table->string('name');
            $table->string('cnic');
            $table->string('dob');
            $table->string('gender');
            $table->string('marital_status');
            $table->string('cell_no');
            $table->string('email');
            $table->string('residence');
            $table->string('educational');
            $table->string('professional');
            $table->string('experience');
            $table->string('reference');
            $table->string('institute_job_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_requests');
    }
}
