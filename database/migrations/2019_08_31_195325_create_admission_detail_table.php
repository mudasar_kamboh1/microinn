<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmissionDetailTable extends Migration
{
    /**
     * Run the migrations.
     * name, phoneNo, email, qualification, type,

     *
     * @return void
     */
    public function up()
    {
        Schema::create('admission_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('phone_no',15)->nullable();
            $table->string('email');
            $table->string('qualification');
            $table->string('type');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admission_detail');
    }
}
