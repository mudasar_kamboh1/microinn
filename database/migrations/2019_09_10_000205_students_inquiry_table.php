<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StudentsInquiryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students_inquiry', function (Blueprint $table) {
            $table->increments('id');
            $table->string('inquiry_no');
//            $table->date('inquiry_date');
            $table->string('inquiry_date');
            $table->string('name');
            $table->string('gender');
            $table->string('contact');
            $table->string('town');
            $table->string('qualification');
            $table->string('total_marks');
            $table->string('obtained_marks');
            $table->string('institute');
            $table->string('course');
            $table->string('availability');
            $table->string('info_source');
            $table->string('note');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students_inquiry');

    }
}
