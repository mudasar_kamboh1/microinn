<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentCourseFee extends Model
{
//    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'students_course_fee';
    protected $guarded = [''];

    public function student(){
        return $this->belongsTo('App\Student');
    }

    public function course(){
        return $this->belongsTo('App\Course');
    }
}
