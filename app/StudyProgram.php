<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudyProgram extends Model
{
    protected $table ='study_program';
    protected $fillable = [
        'degree_id','title'
    ];
}
