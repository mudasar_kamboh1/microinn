<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdmissionDetail extends Model
{
//    use SoftDeletes;
    protected $table = 'admission_detail';
    protected $fillable = ['name','phone_no', 'email', 'qualification','type'];

}
