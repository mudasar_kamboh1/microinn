<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobRequests extends Model
{
//    use SoftDeletes;
    protected $table ='job_requests';
    protected $fillable = [
        'title', 'location', 'photograph', 'name','cnic','dob','gender','marital_status','cell_no','email','residence','educational','professional','experience','reference','institute_job_id'
    ];
}
