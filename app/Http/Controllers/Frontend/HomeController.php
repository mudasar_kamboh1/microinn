<?php

namespace App\Http\Controllers\Frontend;

use App\AdmissionDetail;
use App\ClassProfile;
use App\ContactUs;
use App\DegreeProgram;
use App\Event;
use App\Http\Controllers\Controller;
use App\InstituteJobs;
use App\JobRequests;
use App\Models\OrderAttendee;
use App\Models\OrderDetail;
use App\Models\Ticket;
use App\SiteMeta;
use App\Slider;
use App\AboutContent;
use App\AboutSlider;
use App\Student;
use App\StudentInquiries;
use App\Students;
use App\StudyInstitute;
use App\StudyProgram;
use App\TeacherProfile;
use App\Testimonial;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;


class HomeController extends Controller
{
    public function home()
    {
        $sliders = Slider::orderBy('order', 'asc')->get()->take(10);
        $aboutContent = AboutContent::first();
        $aboutImages = AboutSlider::orderBy('order', 'asc')->get()->take(10);
        $ourService = SiteMeta::where('meta_key', 'our_service_text')->first();
        //for get request
        $testimonials = Testimonial::orderBy('order', 'asc')->get();
        $news = Event::whereType('news')->orderBy('created_at', 'asc')->get()->take(3);
        $events = Event::whereType('events')->orderBy('created_at', 'asc')->get()->take(3);
        $student = Students::orderBy('created_at', 'asc')->get()->take(3);
        return view('frontend.home', compact(
            'sliders',
            'aboutContent',
            'aboutImages',
            'ourService',
            'testimonials',
            'news',
            'events',
            'student'
        ));
    }

    /**
     * subscriber  manage
     * @return mixed
     */
    public function subscribe(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {
            $response = [
                'success' => false,
                'message' => 'Emails is invalid!'
            ];

            return $response;
        }

        $subscriber = SiteMeta::create([
            'meta_key' => 'subscriber',
            'meta_value' => $request->get('email')
        ]);
        $response = [
            'success' => true,
            'message' => 'Thank your for subscribing us.'
        ];

        return $response;
    }

    /* subscriber  manage
     * @return mixed
     */
    public function classProfile()
    {
        $profiles = ClassProfile::all();
        return view('frontend.class', compact('profiles'));
    }

    /* subscriber  manage
     * @return mixed
     */
    public function classDetails($name)
    {
        $profile = ClassProfile::where('slug', $name)->first();
        if (!$profile) {
            aboart(404);
        }
        return view('frontend.class_details', compact('profile'));
    }

    /* Teacher  manage
     * @return mixed
     */
    public function teacherProfile()
    {
        $profiles = TeacherProfile::paginate(env('MAX_RECORD_PER_PAGE_FRONT', 10));
        return view('frontend.teacher', compact('profiles'));
    }

    /* Event  manage
     * @return mixed
     */
    public function event()
    {
        $events = Event::paginate(env('MAX_RECORD_PER_PAGE_FRONT', 10));
        return view('frontend.event', compact('events'));
    }

    /* Event  manage
     * @return mixed
     */
    public function eventDetails($slug)
    {
        $event = Event::where('slug', $slug)->first();
        if (!$event) {
            abort(404);
        }

        return view('frontend.event_details', compact('event'));

    }

    /* Gallery
     * @return mixed
     */
    public function gallery()
    {
        //for get request
        $images = SiteMeta::where('meta_key', 'gallery')->paginate(env('MAX_RECORD_PER_PAGE', 6));
        return view('frontend.gallery', compact('images'));
    }

    /* Contact Us
     * @return mixed
     */
    public function contactUs(Request $request)
    {
        //for save on POST request
        if ($request->isMethod('post')) {

            $validator = Validator::make($request->all(), [
                'email' => 'required|email',
                'name' => 'required|min:2|max:255',
                'message' => 'required|min:5|max:500',
            ]);

            if ($validator->fails()) {
                $response = [
                    'info' => 'error',
                    'message' => 'Input is invalid! Check it again!'
                ];
                return response()->json($response);
            }
            $article = ContactUs::create($request->only(['name','email','message']));
            //now send mail //  webmaster@hrshadhin.me
            $data = [
                'from' => $request->get('email'),
                'to' => env('MAIL_RECEIVER', 'bc120402611@vu.edu.pk'),
                'subject' => "[" . $request->get('name') . "]" . $request->get('subject'),
                'body' => $request->get('message')
            ];
            Mail::send(array(), array(), function ($message) use ($data) {
                $message->to($data['to'], 'bc120402611@vu.edu.pk')
                    ->subject($data['subject'])
                    ->from($data['from'])
                    ->setBody($data['body']);
            });
            $response = [
                'info' => 'success',
                'message' => 'Mail delivered to receiver. Will contact you soon.'
            ];

            return response()->json($response);
        }
        //for get request
        $address = SiteMeta::where('meta_key', 'contact_address')->first();
        $phone = SiteMeta::where('meta_key', 'contact_phone')->first();
        $email = SiteMeta::where('meta_key', 'contact_email')->first();
        $latlong = SiteMeta::where('meta_key', 'contact_latlong')->first();
        return view('frontend.contact_us', compact('address', 'phone', 'email', 'latlong'));

    }

    /* FAQ
     * @return mixed
     */
    public function faq()
    {

        $faqs = SiteMeta::where('meta_key', 'faq')->get();
        return view('frontend.faq', compact('faqs'));

    }

    /* Timeline
     * @return mixed
     */
    public function timeline()
    {

        $timeline = SiteMeta::where('meta_key', 'timeline')->orderBy('id', 'desc')->get();
        return view('frontend.timeline', compact('timeline'));

    }

// *****   About us  *****
    public function aboutUs()
    {
        $about = AboutContent::first();
        /* $faqs = SiteMeta::where('meta_key','faq')->get();
 //        return view('frontend.faq', compact('faqs'));
         return view('frontend.layouts.nav-bar.about_us', compact('faqs'));*/
//        $faqs = SiteMeta::where('meta_key','faq')->get();
//        return view('frontend.faq', compact('faqs'));
        return view('frontend.layouts.nav-bar.about_us', compact('about'));
    }

    public function academicProgram()
    {
        return view('frontend.layouts.nav-bar.academic_programs');
    }

    public function admissionSchedule()
    {
        return view('frontend.layouts.nav-bar.admission_schedule');
    }

    public function applyOnline()
    {
        return view('frontend.layouts.nav-bar.apply_online');
    }

    public function checkAdmission()
    {
        return view('frontend.layouts.nav-bar.');
    }

    public function checkSuitability()
    {
        return view('frontend.layouts.nav-bar.check_for_suitability');
    }

    public function feeCalculator()
    {
        return view('frontend.layouts.nav-bar.fee_calculator');
    }

    public function admissionEligibility()
    {
        return view('frontend.layouts.nav-bar.admission_eligibility');
    }

    public function postSuitability(Request $request)
    {
        $data = $request->all();
        $suitability = new AdmissionDetail();
        $suitability->name = $data['name'];
        $suitability->phone_no = $data['phone_no'];
        $suitability->email = $data['email'];
        $suitability->qualification = $data['qualification'];
        $suitability->type = (!is_null(decrypt($data['route'])) ? decrypt($data['route']) : 'apply-now');
        $suitability->save();

        // form submitted one time
        $request->session()->put('from_submitted', '1');
        return response()->json(['type' => $suitability->type]);
    }

    public function feeStructure()
    {
//                                session()->forget('from_submitted');
        return view('frontend.layouts.nav-bar.fee_structure');
    }

    public function applyUs()
    {
        $institutes = StudyInstitute::all();
        $degree = [];
        return view('frontend.layouts.nav-bar.apply_us', compact('institutes', 'degree'));
    }

    public function eventList()
    {
        $events = Event::whereType('events')->paginate(env('MAX_RECORD_PER_PAGE', 4));
        $news = Event::whereType('news')->orderBy('created_at', 'desc')->get()->take(3);
        return view('frontend.layouts.nav-bar.event_listing', compact('events', 'news'));
    }

    public function newsList()
    {
        $events = Event::whereType('news')->paginate(env('MAX_RECORD_PER_PAGE', 4));
        $news = Event::whereType('news')->orderBy('created_at', 'desc')->get()->take(3);
        return view('frontend.news-listing', compact('events', 'news'));
    }//news-listing-detail

    /* eventDetails
   * @return mixed
   */
    public function eventListingDetail($slug)
    {
        $event = Event::where('slug', $slug)->first();
        $events = Event::all();
        $news = Event::whereType('news')->orderBy('created_at', 'asc')->get()->take(3);

        if (!$event) {
            abort(404);
        }
        return view('frontend.event_listing_detail', compact('event', 'events', 'news'));

    }

    /* eventDetails
 * @return mixed
 */
    public function newsListingDetail($slug)
    {
        $event = Event::where('slug', $slug)->first();
        $events = Event::all();
        $news = Event::whereType('news')->orderBy('created_at', 'asc')->get()->take(3);

        if (!$event) {
            abort(404);
        }
        return view('frontend.news-listing-detail', compact('event', 'events', 'news'));

    }
    public function postInquiry(Request $request)
    {
        $data = $request->all();
        $totalStudent=StudentInquiries::count();
        $suitability = new StudentInquiries();
        $current_date=Carbon::now()->format('d-m-y');
        $inquiry_no='IN-'.str_replace('-','',$current_date).'-'.sprintf('%04d', $totalStudent + 1);
        $suitability->inquiry_no =$inquiry_no ;
        $suitability->inquiry_date =Carbon::now()->format('d-m-y');
        $suitability->name = $data['name'];
        $suitability->gender = $data['gender'];
        $suitability->contact = $data['contact'];
        $suitability->town = $data['town'];
        $suitability->qualification = $data['qualification'];
        $suitability->total_marks = $data['total_marks'];
        $suitability->institute = $data['institute'];
        $suitability->degree = $data['degree'];
        $suitability->study = $data['study'];
        $suitability->availability = $data['availability'];
        $suitability->info_source = $data['info_source'];
        $suitability->save();
        return response()->json(['type' => 'apply-online']);
    }

    public function instituteJobs()
    {
        $jobs = InstituteJobs::paginate(env('MAX_RECORD_PER_PAGE', 2));
//        $news = Event::whereType('news')->orderBy('created_at','desc')->get()->take(3);
        return view('frontend.layouts.nav-bar.job', compact('jobs'));
    }

    public function jobRequest($id)
    {
        $jobs = InstituteJobs::find(decrypt($id));
        return view('frontend.layouts.nav-bar.job_request', compact('jobs'));
    }

    public function postInquiryRequest(Request $request)
    {
        $data = $request->all();
        $suitability = new JobRequests();
        $suitability->institute_job_id = decrypt($data['job_id']);
        $suitability->title = $data['job_title'];
        $suitability->location = $data['job_locator'];
        $suitability->name = $data['name'];
        $suitability->cnic = $data['cnic'];
        $suitability->dob = $data['dob'];
        $suitability->gender = $data['gender'];
        $suitability->marital_status = $data['martial_status'];
        $suitability->cell_no = $data['cell'];
        $suitability->email = $data['email'];
        $suitability->residence = $data['residence'];
        $suitability->educational = $data['educational'];
        $suitability->professional = $data['professional'];
        $suitability->reference = $data['reference'];
        $suitability->experience = $data['experience'];
        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $destinationPath = public_path('storage/job/requests');
            $filename =   time() . "." . $file->getClientOriginalExtension();
            $file->move($destinationPath, $filename);
            $suitability->photograph = $filename;
        }
//        $suitability->photograph = '';
        $suitability->save();
        return response()->json(['type' => 'Inquiry_request']);
    }

    public function showRemain($selectedInstitute = null, $key)
    {
        if (!is_null($selectedInstitute)) {
            if ($key == 'degreeProgram') {
                $degree = DegreeProgram::where('institute_id', $selectedInstitute)->get();//dd($degree);
                $view = (string)view('frontend.layouts.nav-bar.partials.degree_row', compact('degree'));
            } else {
                $degree = StudyProgram::where('degree_id', $selectedInstitute)->get();
                $view = (string)view('frontend.layouts.nav-bar.partials.study_row', compact('degree'));
            }
        } else {
            $degree = [];
            $view = (string)view('frontend.layouts.nav-bar.partials.degree_row', compact('degree'));
        }
        return response()->json([
            'view' => $view,
            'status' => 'success',
        ]);
    }

}
