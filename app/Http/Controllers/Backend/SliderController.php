<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class SliderController extends Controller
{
    public function index()
    {
        $sliders = Slider::orderBy('order', 'asc')->paginate(25);
        if(count($sliders)>10){
            Session::flash('warning','Don\'t add more than 10 slider for better site performance!');
        }
        return view('backend.site.home.slider.list', compact('sliders'));
    }

    public function create()
    {
        return view('backend.site.home.slider.add');
    }

    public function store(Request $request)
    {
        //validate form
        $messages = [
            'image.max' => 'The :attribute size must be under 2MB.',
            'image.dimensions' => 'The :attribute dimensions must be minimum 1900 X 1200.',
        ];
        $this->validate($request, [
            'title' => 'required|min:5|max:255',
            'subtitle' => 'required|min:5|max:255',
            'image' => 'required|mimes:jpeg,jpg,png|max:2048|dimensions:min_width=1900,min_height=1200',

        ], $messages);
        $data = $request->all();

        //
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $destinationPath = public_path('storage/sliders');
            $filename = Auth::user()->id . '-' . time() . "." . $file->getClientOriginalExtension();
            $file->move($destinationPath, $filename);
            $data['image'] = $filename;
        }

        Slider::create($data);

        return redirect()->back()->with('success', 'New slider item created.');
    }

    public function edit($id)
    {
        $slider = Slider::findOrFail($id);
        return view('backend.site.home.slider.edit', compact('slider'));
    }

    public function update(Request $request, $id)
    {
        //validate form
        $messages = [
            'image.max' => 'The :attribute size must be under 2MB.',
            'image.dimensions' => 'The :attribute dimensions must be minimum 1900 X 1200.',
        ];
        $this->validate($request, [
            'title' => 'required|min:5|max:255',
            'subtitle' => 'required|min:5|max:255',
            'image' => 'mimes:jpeg,jpg,png|max:2048|dimensions:min_width=1900,min_height=1200',

        ], $messages);

        $slider = Slider::findOrFail($id);
        $data = $request->all();

        if ($request->hasFile('image')) {

            $file = $request->file('image');
            $destinationPath = public_path('storage/sliders');
            unlink($destinationPath . '/' . $slider->image);
            $filename = Auth::user()->id . '-' . time() . "." . $file->getClientOriginalExtension();
            $file->move($destinationPath, $filename);
            $data['image'] = $filename;
        }

        $slider->fill($data);
        $slider->save();

        return redirect()->route('slider.index')->with('success', 'Slider item updated.');
    }

    public function destroy($id)
    {
        $slider = Slider::findOrFail($id);
        $slider->delete();
        return redirect()->route('slider.index')->with('success', 'Slider item deleted.');
    }
}
