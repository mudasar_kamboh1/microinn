<?php

namespace App\Http\Controllers\Backend;

use App\AdmissionDetail;
use App\DegreeProgram;
use App\InstituteJobs;
use App\StudentInquiries;
use App\StudyInstitute;
use App\StudyProgram;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdmissionDetailController extends Controller
{
    public function index(){
        $admission=AdmissionDetail::paginate(env('MAX_RECORD_PER_PAGE',25));
//        $admissionStatus=AdmissionDetail::get()->unique();//dd($admissionStatus->pluck('type'));//pluck('type');//dd($admissionStatus);
        return view('backend.admission-detail.index',compact('admission'));
    }
    public function admissionType($key = null){
        if (!is_null($key)) {
            $admission = AdmissionDetail::where('type', $key)->paginate(env('MAX_RECORD_PER_PAGE',25));//dd($jobs);
            $view = (string)view('backend.admission-detail.partial.admission_detail_row', compact('admission'));

        } else {
            $jobs = [];
            $view = (string)view('backend.admission-detail.partial.admission_detail_row', compact('admission'));
        }
        return response()->json([
            'view' => $view,
            'status' => 'success',
        ]);
    }

    public function onlineAdmissionList(){
        $admission=StudentInquiries::paginate(env('MAX_RECORD_PER_PAGE',25));
        return view('backend.admission-detail.online-admission.index',compact('admission'));

    }
    public function onlineAdmissionDetail($id){
        $admission=StudentInquiries::find($id);
        $institute=StudyInstitute::find($admission->institute);
        $study=StudyProgram::find($admission->study);
        $degree=DegreeProgram::find($admission->degree);

        return view('backend.admission-detail.online-admission.show',compact('admission','institute','study','degree'));

    }
}
