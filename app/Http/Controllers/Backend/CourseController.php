<?php

namespace App\Http\Controllers\Backend;

use App\Course;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $courses = Course::all();
        return view('backend.courses.list', compact(
            'courses'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $course = null;
        $course_type = null;
        return view('backend.courses.add', compact('course'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules =  [
            'name' => 'required',
            'detail' => 'required',
            'institute_id' => 'required|exists:study_institute,id',
            'campus_id' => 'required|exists:campuses,id',
            'duration' => 'required',
            'certificate' => 'required',
        ];
        $this->validate($request, $rules);

        $data = $request->all();
        $data['code'] = 'CRS-'.date('m').rand(1,100);
        $data['end_date'] = $request->get('end_date') ? Carbon::parse($request->get('end_date'))->format('Y-m-d') : null;
        Course::create($data);

        return redirect()->back()->with('success', 'Course created!');
    }


    public function show(Request $request, $id)
    {
        $course = Course::findorfail($id);
        if($request->wantsJson()){
            return response()->json($course);
        }

        return view('backend.courses.view', compact('course'));


    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param   integer $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $course = Course::findOrFail($id);
        return view('backend.courses.add', compact('course'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param   integer $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $course = Course::findOrFail($id);

        $rules =  [
            'name' => 'required',
            'detail' => 'required',
            'institute_id' => 'required|exists:study_institute,id',
            'campus_id' => 'required|exists:campuses,id',
            'duration' => 'required',
            'certificate' => 'required',
        ];
        $this->validate($request, $rules);

        $data = $request->all();
        $data['end_date'] = $request->get('end_date') ? Carbon::parse($request->get('end_date'))->format('Y-m-d') : null;
        $course->fill($data);
        $course->save();


        return redirect()->back()->with('success', 'Course Updated!');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param   integer $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $course = Course::findOrFail($id);
        $course->delete();

        return redirect()->back()->with('success', 'Course deleted!');
    }
}
