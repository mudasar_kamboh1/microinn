<?php

namespace App\Http\Controllers\Backend;
use App\Event;
use App\Http\Controllers\Controller;

use App\InstituteJobs;
use App\JobRequests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class JobsController extends Controller
{
    public function create(){
        $job = null;
        return view('backend.jobs.create', compact('job'));
    }
    public function store(Request $request){
        $data=$request->all();
        $open_datetime = Carbon::createFromFormat('d/m/Y',$data['open_date']);
        $close_datetime = Carbon::createFromFormat('d/m/Y',$data['close_date']);
        $addJob=new InstituteJobs();
        $addJob->title=$data['title'];
        $addJob->eligibility=$data['eligibility'];
        $addJob->open_date=$open_datetime;
        $addJob->close_date=$close_datetime;
        $addJob->save();
//        InstituteJobs::create($data);
        return redirect()->route('jobs.create')->with('success', 'Jobs added!');
    }
    public function index()
    {
        $jobs = InstituteJobs::paginate(env('MAX_RECORD_PER_PAGE',25));
        return view('backend.jobs.index', compact('jobs'));
    }
    public function edit($id){
        $job = InstituteJobs::findOrFail($id);
        return view('backend.jobs.create', compact('job'));
    }
    public function destroy($id){
        $job = InstituteJobs::findOrFail($id);
        $job->delete();
        return redirect()->route('jobs.index')->with('success', 'Job Request deleted.');
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $open_datetime = Carbon::createFromFormat('d/m/Y',$data['open_date']);
        $close_datetime = Carbon::createFromFormat('d/m/Y',$data['close_date']);
        $addJob= InstituteJobs::findOrFail($id);
        $addJob->title=$data['title'];
        $addJob->eligibility=$data['eligibility'];
        $addJob->open_date=$open_datetime;
        $addJob->close_date=$close_datetime;
        $addJob->save();
        return redirect()->route('jobs.index')->with('success', 'Job information updated.');
    }

}
