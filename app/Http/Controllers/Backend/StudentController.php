<?php

namespace App\Http\Controllers\Backend;

use App\AcademicYear;
use App\Http\Helpers\AppHelper;
use App\IClass;
use App\Registration;
use App\Section;
use App\Student;
use App\StudentCourseFee;
use App\Subject;
use App\Template;
use App\User;
use App\UserRole;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $view = 'backend.student.list';
        if($request->get('type') == 'active'){
            $students = Student::orderBy('id','desc')->paginate(10);
            $view = 'backend.student.list';
        }elseif($request->get('type') == 'inactive'){
            $students = Student::orderBy('id','desc')->paginate(10);
            $view = 'backend.student.list';
        }elseif($request->get('type') == 'studyComplete'){
            $students = Student::has('coursesFee', '>' , 0)->orderBy('id','desc')->paginate(10);
            $view = 'backend.student.studyComplete';
        }elseif($request->get('type') == 'certificate'){
            $students = Student::whereHas('coursesFee.course',function ($q){
                $q->where('passout_status',1);
            })->whereHas('coursesFee.course',function ($q){
                $q->where('certificate','Required');
            })->orderBy('id','desc')->get();
            $view = 'backend.student.certificate';
        }else{
            $students = Student::orderBy('id','desc')->paginate(10);
        }
        return view($view, compact('students'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $classes = IClass::where('status', AppHelper::ACTIVE)
            ->orderBy('order','asc')
            ->pluck('name', 'id');
        $student = null;
        $gender = 1;
        $religion = 1;
        $bloodGroup = 1;
        $nationality = 'Bangladeshi';
        $group = 'None';
        $shift = 'Day';
        $regiInfo = null;
        $sections = [];
        $iclass = null;
        $section = null;
        $acYear = null;
        $esubject = null;
        $csubject = null;
        $electiveSubjects = [];
        $coreSubjects = [];
        $academic_years = [];

        // check for institute type and set gender default value
        $settings = AppHelper::getAppSettings();
        if(isset($settings['institute_type']) && intval($settings['institute_type']) == 2){
            $gender = 2;
        }

        if(AppHelper::getInstituteCategory() == 'college') {
            $academic_years = AcademicYear::where('status', '1')->orderBy('id', 'desc')->pluck('title', 'id');
        }



        return view('backend.student.add', compact(
            'regiInfo',
            'student',
            'gender',
            'religion',
            'bloodGroup',
            'nationality',
            'classes',
            'sections',
            'group',
            'shift',
            'iclass',
            'section',
            'academic_years',
            'acYear',
            'electiveSubjects',
            'coreSubjects',
            'esubject',
            'csubject'
        ));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

//        dd($request->all());
        //validate form
        $messages = [
            'photo.max' => 'The :attribute size must be under 2000kb.',
        ];
        $rules = [
            'name' => 'required|min:5|max:255',
            'photo' => 'mimes:jpeg,jpg,png|max:2000',
            'dob' => 'min:10|max:10',
            'gender' => 'required|integer',
            'religion' => 'nullable|integer',
            'blood_group' => 'nullable|integer',
            'nationality' => 'required|max:50',
            'phone_no' => 'nullable|max:15',
            'note' => 'nullable|max:500',
            'father_name' => 'nullable|max:255',
            'father_phone_no' => 'nullable|max:15',
            'mother_name' => 'nullable|max:255',
            'mother_phone_no' => 'nullable|max:15',
            'guardian' => 'nullable|max:255',
            'guardian_phone_no' => 'nullable|max:15',
            'present_address' => 'nullable|max:500',
            'permanent_address' => 'required|max:500',
            'card_no' => 'nullable|min:4|max:50|unique:students,card_no',
            'shift' => 'nullable|max:15',
            'roll_no' => 'nullable|integer',
            'user_id' => 'nullable|integer',
            'email' => 'nullable|email|max:255|unique:students,email|unique:users,email',
        ];

        $createUser = false;

        if(strlen($request->get('username',''))){
            $rules['email' ] = 'email|max:255|unique:students,email|unique:users,email';
            $createUser = true;

        }

        $this->validate($request, $rules);
        $data = $request->except('balance','amount','fee_plan_date','course_id','reg_fee','admission_fee','test_fee','tuition_fee','discount_fee','net_fee','username','password');
        $course = $request->only('course_id','reg_fee','admission_fee','test_fee','tuition_fee','discount_fee','net_fee','discount_reason');
        if($request->has('study_days')){
            $data['study_days'] = implode(',',$request->get('study_days'));
        }

        if($request->hasFile('photo')) {
            $storagepath = $request->file('photo')->store('/students');
            $fileName = basename($storagepath);
            $data['photo'] = $fileName;
        }


        DB::beginTransaction();
        try {
            //now create user
            if ($createUser) {
                $user = User::create(
                    [
                        'name' => $data['name'],
                        'username' => $request->get('username'),
                        'email' => $data['email'],
                        'phone_no' => $data['phone_no'],
                        'password' => bcrypt($request->get('password')),
                        'remember_token' => null,
                    ]
                );
                //now assign the user to role
                UserRole::create(
                    [
                        'user_id' => $user->id,
                        'role_id' => AppHelper::USER_STUDENT
                    ]
                );
                $data['user_id'] = $user->id;
            }
            // now save employee
            $count= Student::count();
            $data['reg_no'] = str_pad(++$count,3,'0',STR_PAD_LEFT);
            $student = Student::create($data);

            $student->coursesFee()->create($course);

            if($request->has('fee_plan_date')){
                foreach ($request->get('fee_plan_date') as $k =>  $date){
                    $feePlan['due_date'] = $request->get('fee_plan_date')[$k];
                    $feePlan['amount_fee'] = $request->get('amount')[$k];
                    $feePlan['balance']= $request->get('balance')[$k];
//                    $feePlan['course_id'] = $request->get('course_id');
                    $student->feePlans()->attach($request->get('course_id'), $feePlan);
                }
            }

            // now commit the database
            DB::commit();
            $request->session()->flash('message', "Student registration number is ".$data['reg_no']);

            //now notify the admins about this record
            $msg = $data['name']." student added by ".auth()->user()->name;
            $nothing = AppHelper::sendNotificationToAdmins('info', $msg);
            // Notification end

            //invalid dashboard cache
            Cache::forget('studentCount');
            Cache::forget('student_count_by_class');
            Cache::forget('student_count_by_section');

            return redirect()->route('student.create')->with('success', 'Student added!')->withInput();


        }
        catch(\Exception $e){
            DB::rollback();
            dd($e);
            $message = str_replace(array("\r", "\n","'","`"), ' ', $e->getMessage());
            return redirect()->route('student.create')->with("error",$message)->withInput();
        }

        return redirect()->route('student.create');


    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        // if print id card of this student then
        // Do here
        if($request->query->get('print_idcard',0)) {

            $templateId = AppHelper::getAppSettings('student_idcard_template');
            $templateConfig = Template::where('id', $templateId)->where('type',3)->where('role_id', AppHelper::USER_STUDENT)->first();

            if(!$templateConfig){
                return redirect()->route('administrator.template.idcard.index')->with('error', 'Template not found!');
            }

            $templateConfig = json_decode($templateConfig->content);

            $format = "format_";
            if($templateConfig->format_id == 2){
                $format .="two";
            }
            else if($templateConfig->format_id == 3){
                $format .="three";
            }
            else {
                $format .="one";
            }

            //get institute information
            $instituteInfo = AppHelper::getAppSettings('institute_settings');


            $students = Registration::where('id', $id)
                ->where('status', AppHelper::ACTIVE)
                ->with(['student' => function ($query) {
                    $query->select('name', 'blood_group', 'id', 'photo');
                }])
                ->with(['class' => function ($query) {
                    $query->select('name', 'group', 'id');
                }])
                ->select('*')
                ->orderBy('id', 'asc')
                ->get();

            if(!$students){
                abort(404);
            }


            $acYearInfo = AcademicYear::where('id', $students[0]->academic_year_id)->first();
            $session = $acYearInfo->title;
            $validity = $acYearInfo->end_date->format('Y');

            if($templateConfig->format_id == 3){
                $validity = $acYearInfo->end_date->format('F Y');
            }


            $totalStudent = count($students);

            $side = 'both';
            return view('backend.report.student.idcard.'.$format, compact(
                'templateConfig',
                'instituteInfo',
                'side',
                'students',
                'totalStudent',
                'session',
                'validity'
            ));
        }

        //get student
        $student = Student::where('id', $id)->first();
        if(!$student){
            abort(404);
        }
        if($student->user_id){
            $user = User::find($student->user_id);
            $username = $user->username;
        }
        return view('backend.student.view', compact('student', 'username'));


    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student =  Student::find($id);
        if(!$student){
            abort(404);
        }
        $gender = $student->getOriginal('gender');
        $religion = $student->getOriginal('religion');
        $bloodGroup = $student->getOriginal('blood_group');
        $nationality = ($student->nationality != "Pakistani") ? "Other" : "";
        $shift = $student->shift;

        return view('backend.student.add', compact(
            'student',
            'gender',
            'religion',
            'bloodGroup',
            'nationality',
            'shift'
        ));

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){

        $student =  Student::find($id);
        if(!$student){
            abort(404);
        }

        $messages = [
            'photo.max' => 'The :attribute size must be under 2000kb.',
        ];
        $rules = [
            'name' => 'required|min:5|max:255',
            'photo' => 'mimes:jpeg,jpg,png|max:2000',
            'dob' => 'min:10|max:10',
            'gender' => 'required|integer',
            'religion' => 'nullable|integer',
            'blood_group' => 'nullable|integer',
            'nationality' => 'required|max:50',
            'phone_no' => 'nullable|max:15',
            'note' => 'nullable|max:500',
            'father_name' => 'nullable|max:255',
            'father_phone_no' => 'nullable|max:15',
            'mother_name' => 'nullable|max:255',
            'mother_phone_no' => 'nullable|max:15',
            'guardian' => 'nullable|max:255',
            'guardian_phone_no' => 'nullable|max:15',
            'present_address' => 'nullable|max:500',
            'permanent_address' => 'required|max:500',
            'card_no' => 'nullable|min:4|max:20',
            'shift' => 'nullable|max:15',
            'roll_no' => 'nullable|integer',
            'user_id' => 'nullable|integer',
            'email' => 'nullable|email|max:255',
        ];

        $createUser = false;

        if(strlen($request->get('username',''))){
            $rules['email' ] = 'email|max:255|unique:students,email|unique:users,email';
            $createUser = true;

        }

        $this->validate($request, $rules);
        $data = $request->except('balance','amount','fee_plan_date','course_id','reg_fee','admission_fee','test_fee','tuition_fee','discount_fee','net_fee','username','password','oldPhoto','discount_reason');
        $course = $request->only('course_id','reg_fee','admission_fee','test_fee','tuition_fee','discount_fee','net_fee','discount_reason');
        if($request->has('study_days')){
            $data['study_days'] = implode(',',$request->get('study_days'));
        }

        if($request->hasFile('photo')) {
            $storagepath = $request->file('photo')->store('/students');
            $fileName = basename($storagepath);
            $data['photo'] = $fileName;

            //if file change then delete old one
        }


        DB::beginTransaction();
        try {
            //now create user
            if(!$student->user_id && $request->get('user_id', 0)){
                $data['user_id'] = $request->get('user_id');
            }


            // now save student
            $student->fill($data);
            if(($student->isDirty('email') || $student->isDirty('phone_no'))
                && ($student->user_id || isset($data['user_id']))){
                $userId = $data['user_id'] ?? $student->user_id;
                $user = User::where('id', $userId)->first();
                $user->email = $data['email'];
                $user->phone_no = $data['phone_no'];
                $user->save();
            }


            // now save employee
            $count= Student::count();
            $data['reg_no'] = str_pad(++$count,3,'0',STR_PAD_LEFT);
            $student->update($data);

            if(!is_null($student->coursesFee()->first())){
                $student->coursesFee()->update($course);
            }else{
                $student->coursesFee()->create($course);
                if($request->has('fee_plan_date')){
                    foreach ($request->get('fee_plan_date') as $k =>  $date){
                        $feePlan['due_date'] = $request->get('fee_plan_date')[$k];
                        $feePlan['amount_fee'] = $request->get('amount')[$k];
                        $feePlan['balance']= $request->get('balance')[$k];
                        $feePlan['course_id'] = $request->get('course_id');
                        $student->feePlans()->attach($request->get('course_id'), $feePlan);
                    }
                }
            }



            // now commit the database
            DB::commit();
            $request->session()->flash('message', "Student registration number is ".$data['reg_no']);

            //now notify the admins about this record
            $msg = $data['name']." student added by ".auth()->user()->name;
            $nothing = AppHelper::sendNotificationToAdmins('info', $msg);
            // Notification end

            //invalid dashboard cache
            Cache::forget('studentCount');
            Cache::forget('student_count_by_class');
            Cache::forget('student_count_by_section');

            return redirect()->route('student.index')->with('success', 'Student updated!');


        }
        catch(\Exception $e){
            DB::rollback();
            dd($e);
            $message = str_replace(array("\r", "\n","'","`"), ' ', $e->getMessage());
            return redirect()->route('student.create')->with("error",$message)->withInput();
        }

        return redirect()->route('student.create');


    }


    public function passOut(Request $request){
        try {
            $input = $request->all();
            $student_id = $request->get('student_id');
            $student = Student::find($student_id);
            $course = $student->coursesFee()->first();

            $datetime = Carbon::createFromFormat('d/m/Y',$input['pass_date']);
            $course->fill([
                'attendance' => $request->get('attendance'),
                'behaviour' => $request->get('behaviour'),
                'result' => $request->get('result'),
                'pass_date' => $datetime,//$request->get('pass_date'),
            ])->save();
            $request->session()->flash('message', "Successfully updated");

            return back();
        }catch(\Exception $e){
            DB::rollback();
            $message = str_replace(array("\r", "\n","'","`"), ' ', $e->getMessage());
        }
    }


    public function deliverCertificate(Request $request){
        try {
            $input = $request->all();
            $student_id = $request->get('student_id');
            $student = StudentCourseFee::where('student_id' ,  $student_id)
                ->first();
            $datetime = Carbon::now()->format('Y-m-d');
            $student->fill([
                'receive_by' => $request->get('receive_by'),
                'receive_phone' => $request->get('receive_phone'),
                'deliver_user' => Auth::user()->id,
                'deliver_date' => $datetime,
            ])->save();
            $request->session()->flash('message', "Successfully saved!");
            return back();
        }catch(\Exception $e){
            DB::rollback();
            $message = str_replace(array("\r", "\n","'","`"), ' ', $e->getMessage());
            dd($message);
        }
    }
//    public function update(Request $request, $id)
//    {
//        $student =  Student::find($id);
//        if(!$student){
//            abort(404);
//        }
//
//        //validate form
//        $messages = [
//            'photo.max' => 'The :attribute size must be under 2000kb.',
////            'photo.dimensions' => 'The :attribute dimensions min 150 X 150.',
//        ];
//        $rules = [
//            'name' => 'required|min:5|max:255',
//            'photo' => 'mimes:jpeg,jpg,png|max:2000|dimensions:min_width=150,min_height=150',
//            'dob' => 'min:10|max:10',
//            'gender' => 'required|integer',
//            'religion' => 'nullable|integer',
//            'blood_group' => 'nullable|integer',
//            'nationality' => 'required|max:50',
//            'phone_no' => 'nullable|max:15',
//            'note' => 'nullable|max:500',
//            'father_name' => 'nullable|max:255',
//            'father_phone_no' => 'nullable|max:15',
//            'mother_name' => 'nullable|max:255',
//            'mother_phone_no' => 'nullable|max:15',
//            'guardian' => 'nullable|max:255',
//            'guardian_phone_no' => 'nullable|max:15',
//            'present_address' => 'nullable|max:500',
//            'permanent_address' => 'required|max:500',
//            'card_no' => 'nullable|min:4|max:50|unique:students,card_no',
//            'email' => 'nullable|email|max:255|unique:students,email,'.$student->id.'|email|unique:users,email,'.$student->user_id,
//            'shift' => 'nullable|max:15',
//            'roll_no' => 'nullable|integer',
//            'user_id' => 'nullable|integer',
//        ];
//
//        $this->validate($request, $rules);
//
//        $data = $request->all();
//
//        $data['study_days'] = implode(',',$request->study_days);
//
//        $imgStorePath = "public/students/";
//        if($request->hasFile('photo')) {
//            $storagepath = $request->file('photo')->store($imgStorePath);
//            $fileName = basename($storagepath);
//            $data['photo'] = $fileName;
//
//            //if file change then delete old one
//            $oldFile = $request->get('oldPhoto','');
//            if( $oldFile != ''){
//                $file_path = $imgStorePath.'/'.$oldFile;
//                Storage::delete($file_path);
//            }
//        }
//        else{
//            $data['photo'] = $request->get('oldPhoto','');
//        }
//
//        DB::beginTransaction();
//        try {
//
//            if(!$student->user_id && $request->get('user_id', 0)){
//                $data['user_id'] = $request->get('user_id');
//            }
//
//
//            // now save student
//            $student->fill($data);
//            if(($student->isDirty('email') || $student->isDirty('phone_no'))
//                && ($student->user_id || isset($data['user_id']))){
//                $userId = $data['user_id'] ?? $student->user_id;
//                $user = User::where('id', $userId)->first();
//                $user->email = $data['email'];
//                $user->phone_no = $data['phone_no'];
//                $user->save();
//            }
//            $student->save();
//            // now commit the database
//            DB::commit();
//            return redirect()->route('student.index')->with('success', 'Student updated!');
//
//
//        }
//        catch(\Exception $e){
//            DB::rollback();
//            $message = str_replace(array("\r", "\n","'","`"), ' ', $e->getMessage());
////            dd($message);
//        }
//
//        return redirect()->route('student.edit', $student->id)->with("error",$message);;
//    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $registration = Registration::find($id);
        if(!$registration){
            abort(404);
        }
        $student =  Student::find($registration->student_id);
        if(!$student){
            abort(404);
        }

        $message = 'Something went wrong!';
        DB::beginTransaction();
        try {

            $registration->delete();
            $student->delete();
            if($student->user_id){
                $user = User::find($student->user_id);
                $user->delete();
            }
            DB::commit();


            //now notify the admins about this record
            $msg = $student->name." student deleted by ".auth()->user()->name;
            $nothing = AppHelper::sendNotificationToAdmins('info', $msg);
            // Notification end
            //invalid dashboard cache
            Cache::forget('studentCount');
            Cache::forget('student_count_by_class');
            Cache::forget('student_count_by_section');

            return redirect()->route('student.index')->with('success', 'Student deleted.');

        }
        catch(\Exception $e){
            DB::rollback();
            $message = str_replace(array("\r", "\n","'","`"), ' ', $e->getMessage());
        }
        return redirect()->route('student.index')->with('error', $message);

    }

    /**
     * status change
     * @return mixed
     */
    public function changeStatus(Request $request, $id=0)
    {

        $registration = Registration::find($id);
        if(!$registration){
            return [
                'success' => false,
                'message' => 'Record not found!'
            ];
        }
        $student =  Student::find($registration->student_id);
        if(!$student){
            return [
                'success' => false,
                'message' => 'Record not found!'
            ];
        }

        $student->status = (string)$request->get('status');
        $registration->status = (string)$request->get('status');
        if($student->user_id){
            $user = User::find($student->user_id);
            $user->status = (string)$request->get('status');
        }

        $message = 'Something went wrong!';
        DB::beginTransaction();
        try {

            $registration->save();
            $student->save();
            if($student->user_id) {
                $user->save();
            }
            DB::commit();

            return [
                'success' => true,
                'message' => 'Status updated.'
            ];


        }
        catch(\Exception $e){
            DB::rollback();
            $message = str_replace(array("\r", "\n","'","`"), ' ', $e->getMessage());
        }

        return [
            'success' => false,
            'message' => $message
        ];


    }

    public function changeCertificateStatus(Request $request, $id=0)
    {

        $student =  Student::find($id);
        if(!$student){
            return [
                'success' => false,
                'message' => 'Record not found!'
            ];
        }

        $student->certificate_status = 1;

        $message = 'Something went wrong!';
        DB::beginTransaction();
        try {
            $student->save();
            DB::commit();

            return [
                'success' => true,
                'message' => 'Status updated.'
            ];


        }
        catch(\Exception $e){
            DB::rollback();
            $message = str_replace(array("\r", "\n","'","`"), ' ', $e->getMessage());
        }

        return [
            'success' => false,
            'message' => $message
        ];


    }

    public function changeCertificateStatus1(Request $request, $id=0)
    {

        $student =  Student::find($id);
        if(!$student){
            return [
                'success' => false,
                'message' => 'Record not found!'
            ];
        }

        $student->certificate_status = 2;
        $message = 'Something went wrong!';
        DB::beginTransaction();
        try {
            $student->save();
            DB::commit();

            return [
                'success' => true,
                'message' => 'Status updated.'
            ];


        }
        catch(\Exception $e){
            DB::rollback();
            $message = str_replace(array("\r", "\n","'","`"), ' ', $e->getMessage());
        }

        return [
            'success' => false,
            'message' => $message
        ];


    }

    /**
     * Get student list by filters
     */
    public function studentListByFitler(Request $request) {
        $classId = $request->query->get('class',0);
        $sectionId = $request->query->get('section',0);
        $acYear = $request->query->get('academic_year',0);

        if(AppHelper::getInstituteCategory() != 'college') {
            $acYear = AppHelper::getAcademicYear();
        }

        $students = Registration::where('academic_year_id', $acYear)
            ->where('class_id', $classId)
            ->where('section_id', $sectionId)
            ->where('status', AppHelper::ACTIVE)
            ->with(['student' => function ($query) {
                $query->select('name','id');
            }])
            ->select('id','roll_no','student_id')
            ->orderBy('roll_no','asc')
            ->get();

        return response()->json($students);

    }
}
