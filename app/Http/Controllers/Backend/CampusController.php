<?php

namespace App\Http\Controllers\Backend;

use App\Campus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class CampusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $campuses = Campus::all();
        return view('backend.campus.list', compact(
            'campuses'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $campus = null;
        $campus_type = null;
        return view('backend.campus.add', compact('campus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules =  [
            'name' => 'required',
            'address' => 'required',
        ];
        $this->validate($request, $rules);

        $data = $request->only('name','address');
        $data['code'] = 'CAM-'.rand(100,200);
        Campus::create($data);

        return redirect()->back()->with('success', 'Campus created!');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param   integer $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $campus = Campus::findOrFail($id);
        return view('backend.campus.add', compact('campus'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param   integer $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $campus = Campus::findOrFail($id);

        $rules =  [
            'name' => 'required',
            'address' => 'required',
        ];
        $this->validate($request, $rules);

        $data = $request->only('name','address');
        $campus->fill($data);
        $campus->save();


        return redirect()->back()->with('success', 'Campus Updated!');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param   integer $id
     * @return \Illuminate\Http\Response
     */
//    public function destroy($id)
//    {
//        $campus = Campus::findOrFail($id);
//        $campus->delete();
//
//        return redirect()->back()->with('success', 'Campus deleted!');
//    }
}
