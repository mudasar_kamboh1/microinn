<?php

namespace App\Http\Controllers\Backend;

use App\Students;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class StudentsController extends Controller
{

    public function index(){
        $students=Students::paginate(env('MAX_RECORD_PER_PAGE',25));
        return view('backend.students.index',compact('students'));
    }
    public function create(Request $request){
        $students=null;

        return view('backend.students.create',compact('students'));
    }
    public function store(Request $request){
        $studentRequest=$request->only('name','description');//dd($studentRequest);
        $fileName = null;

        if($request->hasFile('image')) {
            $file = $request->file('image');
            $destinationPath = public_path('storage/students');
            $filename =   time() . "." . $file->getClientOriginalExtension();
            $file->move($destinationPath, $filename);
            $studentRequest['image'] = $filename;
        }
        Students::create($studentRequest);
           return redirect()->back()->with('success', 'New Student added.');
    }
    public function edit($id){
        $students=Students::find($id);
        return view('backend.students.create',compact('students'));
    }
    public function update(Request $request, $id){
        $data = $request->all();
        $students=Students::find($id);
        $filename = null;
        if($request->hasFile('image')) {
            if($students->image){
                $file_path = "storage/students/".$students->image;
                Storage::delete($file_path);
            }
            $file = $request->file('image');
            $destinationPath = public_path('storage/students');
            $filename =   time() . "." . $file->getClientOriginalExtension();
            $file->move($destinationPath, $filename);
            $data['image'] = $filename;
        }
        $students->fill($data);
        $students->save();
        return redirect()->route('students.index')->with('success', 'Students information updated.');
    }
    public function destroy($id){
        $request = Students::findOrFail($id);
        $request->delete();
        return redirect()->route('students.index')->with('success', 'Students deleted.');
    }
}
