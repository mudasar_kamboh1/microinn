<?php

namespace App\Http\Controllers\Backend;

use App\Event;
use App\JobRequests;
use PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

class JobRequestController extends Controller
{
    public function index(){
        $requestJobs=JobRequests::paginate(env('MAX_RECORD_PER_PAGE',25));
        return view('backend.request_job.index', compact('requestJobs'));
    }
    public function edit($id){
        $request = JobRequests::findOrFail($id);
        return view('backend.request_job.edit', compact('request'));
    }
    public function destroy($id){
        $request = JobRequests::findOrFail($id);
        $request->delete();
        return redirect()->route('job-requests.index')->with('success', 'Job Request deleted.');
    }
    public function show($id){
        $request = JobRequests::findOrFail($id);
//        $filename = asset('storage/job/requests/').$request->photograph;
//        $pdf = PDF::loadView('backend.request_job.show' , compact('request'));
        return view('backend.request_job.show', compact('request'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $jobRequest = JobRequests::findOrFail($id);
        $filename = null;
        if($request->hasFile('photograph')) {
            if($jobRequest->photograph){
                $file_path = "storage/job/requests/".$jobRequest->photograph;
                Storage::delete($file_path);
            }
            $file = $request->file('photograph');
            $destinationPath = public_path('storage/job/requests');
            $filename =   time() . "." . $file->getClientOriginalExtension();
            $file->move($destinationPath, $filename);
            $data['photograph'] = $filename;
        }
        $jobRequest->fill($data);
        $jobRequest->save();
        return redirect()->route('job-requests.index')->with('success', 'Event information updated.');
    }

}
