<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Hrshadhin\Userstamps\UserstampsTrait;
use App\Http\Helpers\AppHelper;
use Illuminate\Support\Arr;


class Student extends Model
{
    use SoftDeletes;
    use UserstampsTrait;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [''];

    public function registration()
    {
        return $this->hasMany('App\course_id', 'student_id');
    }
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    public function getGenderAttribute($value)
    {
        return Arr::get(AppHelper::GENDER, $value);
    }

    public function getReligionAttribute($value)
    {
        return Arr::get(AppHelper::RELIGION, $value);
    }

    public function getBloodGroupAttribute($value)
    {
        return Arr::get(AppHelper::BLOOD_GROUP, $value);
    }

    public function coursesFee()
    {
        return $this->hasMany('App\StudentCourseFee', 'student_id');
    }

    public function feePlans()
    {
        return $this->belongsToMany('App\Course', 'students_fee_plan','student_id','course_id')->withPivot('amount_fee', 'balance','due_date','status','created_at','updated_at');
    }
}
