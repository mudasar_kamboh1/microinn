<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DegreeProgram extends Model
{
    protected $table ='degree_program';
    protected $fillable = [
        'institute_id','title'
    ];
}
