<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InstituteJobs extends Model
{
//    use SoftDeletes;
    protected $dates = [
        'open_date','close_date'
    ];
    protected $table ='institute_jobs';
    protected $fillable = [
        'title', 'eligibility', 'open_date', 'close_date'
    ];
}
