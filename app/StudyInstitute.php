<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudyInstitute extends Model
{
    protected $table ='study_institute';
    protected $fillable = [
        'institute'
    ];
}
