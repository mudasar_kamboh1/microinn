<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
//    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [''];


    public function institute(){
        return $this->belongsTo('App\StudyInstitute');
    }

    public function campus(){
        return $this->belongsTo('App\Campus');
    }
}
