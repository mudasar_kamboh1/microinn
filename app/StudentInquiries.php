<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudentInquiries extends Model
{
//    use SoftDeletes;
    protected $table = 'students_inquiry';
    protected $fillable = ['inquiry_no','inquiry_date', 'name', 'gender','contact','town', 'qualification','total_marks','institute', 'degree','study','availability','info_source'
    ];
}
