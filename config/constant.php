<?php

return [
    'route_detail' => [
        'check_suitability' => 'Check Suitability',
        'admission_eligibility' => 'Admission Eligibility',
        'academic_program' => 'Academic Program',
        'fee_calculator' => 'Fee Calculator',
        'admission_schedule' => 'Admission Schedule',
        'fee_structure' => 'Fee Structure',
        'apply_online' => 'Apply Online',
        'apply_with_us' => 'Apply With Us',
    ]
];