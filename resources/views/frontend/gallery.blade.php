@extends('frontend.layouts.master')

@section('pageTitle') @lang('site.menu_home') @endsection
@section('extraStyle')
    <link type="text/css" rel="stylesheet" href="{{ asset('/frontend/rs-plugin/css/settings.css') }}"/>
    <style>
        .img-hover-zoom {
            height: 300px; /* [1.1] Set it as per your need */
            overflow: hidden; /* [1.2] Hide the overflowing of child elements */
        }

        /* [2] Transition property for smooth transformation of images */
        .img-hover-zoom img {
            transition: transform .5s ease;
        }

        /* [3] Finally, transforming the image when container gets hovered */
        .img-hover-zoom:hover img {
            transform: scale(1.5);
        }
    </style>
@endsection
@section('pageContent')
    <div class="inner-banner">
        <div class="opacity">
            <div class="container">
                <h2>Gallery</h2>
            </div> {{--<!-- /.container -->--}}
        </div>{{-- <!-- /.opacity -->--}}
    </div>

    <div class="our-portfolio">
        <div class="container">
            <div class="row" id="mixitUp-item">
                @foreach($images as $image)
                    <div class="col-sm-4 col-xs-6 mix presentation workshop" style="display: inline-block;"
                         data-bound="">
                        <div class="single-item">
                            <div class="img-hover-zoom">
                                <img src="{{asset('images/gallery/'.$image->meta_value)}}" alt="">
                                {{--                        <img src="images/portfolio/1.jpg" alt="">--}}
                            </div>

                        </div> <!-- /.single-item -->
                    </div>
                @endforeach
                {{--            <!-- /.col-md-6 -->--}}
            </div> {{--<!-- /.row -->--}}
            <ul class="theme-pagination clearfix">
                <li>
                    <span class="tran3s">{{ $images->links() }}</span>
                </li>
            </ul>
        </div> {{--<!-- /.container -->--}}
    </div>
@endsection
