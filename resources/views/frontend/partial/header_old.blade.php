<header class="theme-menu-wrapper menu-style-one">
    <div class="container">
        <div class="header-wrapper clearfix">
            <!-- Logo -->
            <div class="logo float-left tran4s"><a href="index-2.html"><img src="{{ asset('images/logo/logo.png')}}" width="212px" alt="Logo"></a></div>

            <!-- ============================ Theme Menu ========================= -->
            <nav class="theme-main-menu float-right navbar" id="mega-menu-wrapper">
                <!-- Brand and toggle get grouped for better mobile display -->
               <div class="navbar-header">
                 <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
                   <span class="sr-only">Toggle navigation</span>
                   <span class="icon-bar"></span>
                   <span class="icon-bar"></span>
                   <span class="icon-bar"></span>
                 </button>
               </div>
               <!-- Collect the nav links, forms, and other content for toggling -->
               <div class="collapse navbar-collapse" id="navbar-collapse-1">
                    <ul class="nav">
                        <li class="dropdown-holder menu-list"><a href="index-2.html" class="tran3s">Home</a>
                            <ul class="sub-menu">
                                <li><a href="index-2.html">Home version one</a></li>
                                <li><a href="index-3.html">Home version Two</a></li>
                            </ul>
                        </li>
                        <li class="dropdown-holder menu-list"><a href="#" class="tran3s">Course</a>
                            <ul class="sub-menu">
                                <li><a href="course-grid.html">Course grid</a></li>
                                <li><a href="course-list.html">Course List</a></li>
                                <li><a href="course-details.html">Course Details</a></li>
                                <li class="dropdown-holder"><a href="#">Third Level menu</a>
                                    <ul class="second-sub-menu">
                                        <li><a href="#">menu one</a></li>
                                        <li><a href="#">menu two</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown-holder menu-list"><a href="#" class="tran3s">Pages</a>
                            <ul class="sub-menu width-half">
                                <li><a href="about-us.html">About Us</a></li>
                                <li><a href="our-teacher.html">Our teacher</a></li>
                                <li><a href="teachers-profile.html">teachers profile</a></li>
                                <li><a href="shop.html">Shop page</a></li>
                                <li><a href="shop-details.html">Shop Details</a></li>
                                <li><a href="portfolio-3-column.html">portfolio 3 column</a></li>
                                <li><a href="portfolio-4-column.html">portfolio 4 column</a></li>
                                <li><a href="pricing-plan.html">Pricing Plan</a></li>
                                <li><a href="faq.html">Faq Page</a></li>
                                <li><a href="404.html">Error Page</a></li>
                                <li><a href="sign-up.html">Registration</a></li>
                                <li class="dropdown-holder"><a href="#">Third Level menu</a>
                                    <ul class="second-sub-menu">
                                        <li><a href="#">menu one</a></li>
                                        <li><a href="#">menu two</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown-holder menu-list"><a href="#" class="tran3s">Event</a>
                            <ul class="sub-menu">
                                <li><a href="event-list.html">Event List</a></li>
                                <li><a href="event-grid.html">Event Grid</a></li>
                                <li><a href="event-details.html">Event Single</a></li>
                            </ul>
                        </li>
                        <li class="dropdown-holder menu-list"><a href="#" class="tran3s">Blog</a>
                            <ul class="sub-menu">
                                <li><a href="blog-grid.html">Blog grid</a></li>
                                <li><a href="blog-list.html">Blog List</a></li>
                                <li><a href="blog-details.html">blog Details</a></li>
                            </ul>
                        </li>
                        <li><a href="contact-us.html" class="tran3s">Contact Us</a></li>
                        <li class="login"><button class="tran3s" data-toggle="modal" data-target=".signInModal"><i class="flaticon-lock"></i> Login</button></li>
                    </ul>
               </div><!-- /.navbar-collapse -->
            </nav> <!-- /.theme-main-menu -->
        </div> <!-- /.header-wrapper -->
    </div>
</header> <!-- /.theme-menu-wrapper -->
