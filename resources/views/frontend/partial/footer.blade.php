<!-- END PAGE JS-->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="footer-logo">
                    <a href="#"><img src="{{ asset('images/logo/logo.png')}}" width="212px" alt="Logo"></a>
                    <p>It was some time before he obtained any answer, and the reply, when made, was unpropitious.</p>
                    <ul>
                        <li><a href="#" class="tran3s"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="#" class="tran3s"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li><a href="#" class="tran3s"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                        <li><a href="#" class="tran3s"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-2 col-sm-3 footer-list">
                <h6>Links</h6>
                <ul>
                    <li><a href="{{ url('/about-us') }}" class="tran3s">About Us</a></li>
                    {{--<li><a href="{{ url('/') }}" class="tran3s">Blog</a></li>--}}
                    {{--<li><a href="{{ url('/') }}" class="tran3s">Become a Teacher</a></li>--}}
                    {{--<li><a href="{{ url('/') }}" class="tran3s">Faq &amp; Plicy</a></li>--}}
                    <li><a href="{{ url('/job') }}" class="tran3s">Job</a></li>
                    <li><a href="{{ url('/events') }}" class="tran3s">Events</a></li>
                </ul>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 footer-list">
                {{--<h6>Subscribe Us</h6>--}}
                {{--<p>This sounded a very good reason, and Alice was quite pleased.</p>--}}
                {{--<form action="#">--}}
                    {{--<input type="text" placeholder="Your Email">--}}
                    {{--<button class="tran3s s-bg-color"><i class="flaticon-envelope-back-view-outline"></i></button>--}}
                {{--</form>--}}
                <h6>Links</h6>
                <ul>
                    <li><a href="{{ url('/news') }}" class="tran3s">News</a></li>
                    <li><a href="{{ url('/gallery') }}" class="tran3s">Gallery</a></li>
                    <li><a href="{{ url('/contact-us') }}" class="tran3s">Contact</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="bottom-footer">
        <div class="container">
            <p class="text-center">&copy; 2019 <a href="{{ url('/') }}" class="tran3s s-color">Micro Inn</a>. All rights
                reserved</p>
        </div>
    </div>
</footer>


<div class="modal fade signInModal theme-modal-box" role="dialog">
    <div class="modal-dialog">
        {{--<!-- Modal content-->--}}
        <div class="modal-content">
            <div class="modal-body">
                {{--<h3>Login with Social Networks</h3>--}}
                <ul class="clearfix">
                    {{--<li class="float-left"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i> facebook</a></li>--}}
                    {{--<li class="float-left"><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i> Google</a></li>--}}
                    {{--<li class="float-left"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i> Twitter</a></li>--}}
                    <a href="#" style="width:250px;display: table;margin: auto;"> <img src="{{ asset('images/logo/logo.png') }}"
                                                                           alt=""></a>
                </ul>
                @if (Session::has('success') || Session::has('error') || Session::has('warning'))
                    <div class="row">
                        <div class="col-md-4 col-md-offset-4">
                            <div class="alert @if (Session::has('success')) alert-success @elseif(Session::has('error')) alert-danger @else alert-warning @endif alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                @if (Session::has('success'))
                                    <h5><i class="icon fa fa-check"></i>{{ Session::get('success') }}</h5>
                                @elseif(Session::has('error'))
                                    <h5><i class="icon fa fa-ban"></i>{{ Session::get('error') }}</h5>
                                @else
                                    <h5><i class="icon fa fa-warning"></i>{{ Session::get('warning') }}</h5>
                                    @endif
                                    </h5>
                            </div>
                        </div>
                    </div>
                @endif
                <form action="{{URL::Route('login')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <h3>Login with Site Account</h3>
                    <div class="wrapper">
                        <input type="text" name="username" placeholder="Username" required minlength="5" maxlength="255"
                               autofocus>
                        <input type="password" placeholder="Password" name="password" required minlength="6"
                               maxlength="255">
                        <ul class="clearfix">
                            <li class="float-left">
                                <input type="checkbox" id="remember">
                                <label for="remember">Remember Me</label>
                            </li>
                            {{--<li class="float-right"><a href="#" class="s-color">Lost Your Password?</a></li>--}}
                        </ul>
                        <button class="p-bg-color hvr-trim">Login</button>
                    </div>
                </form>
                {{--<div><a href="{{url('login')}}" class="p-color tran3s">Not an account?? Sign Up</a></div>--}}
                {{--<div><a href="JavaScript:Void(0);" class="p-color tran3s">Not an account?? Sign Up</a></div>--}}
            </div>
        </div>
    </div>
</div>
