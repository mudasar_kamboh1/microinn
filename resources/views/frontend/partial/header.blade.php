<header class="theme-menu-wrapper menu-style-one">
    <div class="header-wrapper clearfix">
        <div class="container">
            <!-- Logo -->
            <div class="logo float-left tran4s"><a href="{{URL::route('home')}}"><img src="{{ asset('images/logo/logo.png')}}" width="212px" alt="Logo"></a></div>

            <!-- ============================ Theme Menu ========================= -->
            <nav class="theme-main-menu float-right navbar" id="mega-menu-wrapper">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-collapse-1">
                    <ul class="nav">
                        {{-- <li class="dropdown-holder menu-list"><a href="index-2.html" class="tran3s">Home</a>
                             <ul class="sub-menu">
                                 <li><a href="index-2.html">Home version one</a></li>
                                 <li><a href="index-3.html">Home version Two</a></li>
                             </ul>
                         </li>--}}
                        <li><a href="{{URL::route('home')}}" class="tran3s">Home</a></li>

                        <li><a href="{{URL::route('site.about_us')}}" class="tran3s">About Us</a></li>

                        <li class="dropdown-holder menu-list"><a hresf="#" class="tran3s">Admissions in VU</a>
                            <ul class="sub-menu">
                                <li><a href="{{URL::route('site.check_for_suitability')}}">Check your suitability</a></li>
                                <li><a href="{{url('admission-eligibility')}}">Check Admission Eligibility</a></li>
                                <li><a href="{{URL::route('site.academic_program')}}">Academic Programs</a></li>
                                <li><a href="{{URL::route('site.fee_calculator')}}">Fee calculator</a></li>
                                <li><a href="{{URL::route('site.admission_schedule')}}">Admission Schedule</a></li>
                                <li><a href="{{URL::route('site.apply_online')}}">Apply Online</a></li>
                            </ul>
                        </li>
                        <li class="dropdown-holder menu-list"><a href="#" class="tran3s">Admissions with us</a>
                            <ul class="sub-menu ">
                                {{--<li><a href="{{URL::route('site.check_for_suitability')}}">Why to choose MicroInn</a></li>--}}
                                {{--<li><a href="{{URL::route('site.check_for_suitability')}}">Courses offered at sheikhupura</a></li>--}}
                                {{--<li><a href="{{URL::route('site.check_for_suitability')}}">Courses offered at Shahdara</a></li>--}}
                                <li><a href="{{URL::route('site.fee_structure')}}">Fee Schedule</a></li>
                                <li><a href="{{URL::route('site.apply_with_us')}}">Apply With Us</a></li>
                            </ul>
                        </li>
                        {{--<li><a target="_blank" href="http://microinn.edu.pk"  class="tran3s">Campus Life</a></li>--}}
                        {{--<li><a href="{{URL::route('site.event_list')}}"  class="tran3s">Events</a></li>--}}
                        {{--<li><a href="{{URL::route('site.contact_us_view')}}" class="tran3s">Jobs</a></li>--}}
                        <li><a href="{{URL::route('site.contact_us_view')}}" class="tran3s">Contact Us</a></li>
                        <li class="login"><button class="tran3s" data-toggle="modal" data-target=".signInModal"><i class="flaticon-lock"></i> Login</button></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </nav> <!-- /.theme-main-menu -->
        </div> <!-- /.header-wrapper -->
    </div>
</header> <!-- /.theme-menu-wrapper -->
