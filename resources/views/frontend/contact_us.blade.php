@extends('frontend.layouts.master')
@section('pageTitle') @lang('site.menu_home') @endsection
@section('extraStyle')
    <link type="text/css" rel="stylesheet" href="{{ asset('/frontend/rs-plugin/css/settings.css') }}"/>
@endsection
@section('pageContent')
    <div class="inner-banner">
        <div class="opacity">
            <div class="container">
                <h2>Contact Us</h2>
            </div>
        </div>
    </div>
    <div class="container contact-us-page">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12 wow fadeInLeft">
                <div class="contact-us-form">
                    <h2>Send a Message</h2>
                    <form action="{{url('contact-us')}}" class="form-validation" autocomplete="off" method="post" id="contact-form">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="single-input">
                                    <input type="text" placeholder="You Name*" name="name" id="contact-name">
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="single-input">
                                    <input type="email" placeholder="Enter Email here*" name="email" id="contact-email">
                                </div>
                            </div>
                            {{--<div class="col-sm-6 col-xs-12">
                                <div class="single-input">
                                    <input type="text" placeholder="Phone Number" name="phone_no" id="contact-phone_no" >
                                </div>
                            </div>--}}
                            <div class="col-xs-12">
                                <div class="single-input">
                                    <textarea placeholder="Your Message" name="message" id="message"></textarea>
                                </div>
                            </div>
                        </div>
                        <input type="submit" value="Send Message" id="eligibilty" class="tran3s p-bg-color">
                    </form>

                    <div class="alert-wrapper" id="alert-success">
                        <div id="success">
                            <button class="closeAlert"><i class="fa fa-times" aria-hidden="true"></i></button>
                            <div class="wrapper">
                                <p>Your message was sent successfully.</p>
                            </div>
                        </div>
                    </div>
                    <div class="alert-wrapper" id="alert-error">
                        <div id="error">
                            <button class="closeAlert"><i class="fa fa-times" aria-hidden="true"></i></button>
                            <div class="wrapper">
                                <p>Sorry!Something Went Wrong.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-sm-12 col-xs-12 wow fadeInRight">
                <div class="contactUs-address">
                    <h2>Contact Information</h2>
                    <div class="single-address clearfix">
                        <div class="icon float-left"><i class="flaticon-placeholder"></i></div>
                        <div class="text float-left">
                            <h6>Address</h6>
                            <span>16 Shami Road, Civil Lines, Sheikhupura (PSKP01)</span>
                        </div>
                    </div>
                    <div class="single-address clearfix">
                        <div class="icon float-left"><i class="flaticon-envelope"></i></div>
                        <div class="text float-left">
                            <h6>Email</h6>
                            <span>PSKP01@VU.EDU.PK</span>
                        </div>
                    </div>
                    <div class="single-address clearfix">
                        <div class="icon float-left"><i class="flaticon-phone-call"></i></div>
                        <div class="text float-left">
                            <h6>Phone &amp; Landlines</h6>
                            <span>056-3613999, 056-3614999, 0333-4440552-3, 0333-1210894-5</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br><br>
@endsection

@section('extraScript')
    <script type="text/javascript" src="{{ asset('js/validate.js')}}"></script>
@endsection
