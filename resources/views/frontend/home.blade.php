@extends('frontend.layouts.master')

@section('pageTitle') @lang('site.menu_home') @endsection
@section('extraStyle')
    <link type="text/css" rel="stylesheet" href="{{ asset('/frontend/rs-plugin/css/settings.css') }}" />
@endsection

@section('pageContent')
    <div id="theme-main-banner" class="banner-one">
        @foreach($sliders as $slider)
            <div data-src="{{asset('storage/sliders/'.$slider->image)}}">
                <div class="camera_caption">
                    <div class="container text-center">
                        <h3 class="wow fadeInUp animated">{{$slider->title}}</h3>
                        <h1 class="wow fadeInUp animated" data-wow-delay="0.2s">Designed to <span class="p-color">{{$slider->subtitle}}</span></h1>
                        {{--<a href="course-2-column.html" class="tran3s wow fadeInLeft animated banner-button" data-wow-delay="0.3s">Start Learn Now</a>--}}
                        <a href="{{ url('apply-online') }}" class="tran3s wow fadeInRight animated button-one banner-button hvr-trim" data-wow-delay="0.3s">Apply Now</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="find-course-block">
        <div class="container">
            <div class="theme-title text-center">
                <h6>Choose Your Course</h6>
                <h2>We Have Tones of Course for You!!</h2>
            </div>
            <div class="row">
                <div class="col-md-4 col-xs-12 registration-banner">
                    <div class="registration-form single-block tran3s" style="width: 100%;">
                        <form class="form-validation" method="post" action="{{url('/post-suitability')}}" id="eligible-form">
                            <meta name="_token" content="{{csrf_token()}}" />
                            {{ csrf_field() }}
                            <input type="hidden" name="route" value="{{ encrypt(request()->segment(1)) }}">
                            <h2>Apply Now</h2>
                            <hr>
                            <div class="form-wrapper">
                                <input type="text" placeholder="Full Name" id="contact-name" value="" name="name">
                                <input type="email" placeholder="Email" id="contact-email" value="" name="email">
                                <input type="text" placeholder="Mobile Number" value=""  id="contact-phone_no" name="phone_no">
                                <input type="text" placeholder="Qualification" value=""  id="contact-qualification" name="qualification">
                                <button type="button" id="submit_request" class="tran3s hvr-trim">Send Request!</button>
                                <div id="success_message" class="ajax_response" style="float:left;color: green;"></div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-8 col-xs-12">
                    <div class="single-block float-left tran3s">
                        <i class="flaticon-photo-camera"></i>
                        <h5>Photography</h5>
                        <p>There are many variations of passages of check Lorem Ipsum availablebut the majority have suffered alteration</p>
                        <a href="course-details.html" class="tran3s"><i class="flaticon-arrows"></i></a>
                    </div>
                    <div class="single-block float-left tran3s">
                        <i class="flaticon-briefcase"></i>
                        <h5>Online Marketing</h5>
                        <p>There are many variations of passages of check Lorem Ipsum availablebut the majority have suffered alteration</p>
                        <a href="course-details.html" class="tran3s"><i class="flaticon-arrows"></i></a>
                    </div>
                    <div class="single-block float-left tran3s">
                        <i class="flaticon-compose"></i>
                        <h5>Illustrator &amp; Vector</h5>
                        <p>There are many variations of passages of check Lorem Ipsum availablebut the majority have suffered alteration</p>
                        <a href="course-details.html" class="tran3s"><i class="flaticon-arrows"></i></a>
                    </div>
                    <div class="single-block float-left tran3s">
                        <i class="flaticon-coding"></i>
                        <h5>Design &amp; Development</h5>
                        <p>There are many variations of passages of check Lorem Ipsum availablebut the majority have suffered alteration</p>
                        <a href="course-details.html" class="tran3s"><i class="flaticon-arrows"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="our-blog">
        <div class="container">
            <div class="col-md-8 theme-sidebar">
                <div class="sidebar-recent-news">
                    <h4>Recent News</h4>
                    <div class="row">
                        @foreach($news as $newS)
                            <div class="col-md-4 col-sm-6">
                                <div class="single-blog">
                                    <div class="image"><img src="@if($newS->cover_photo){{ asset('images/event/'.$newS->cover_photo) }}@else {{ asset('images/default/news.jpg')}} @endif" alt=""></div>
                                    <h5><a class="tran3s">{{$newS->title}}</a></h5>
                                    <p>
                                        {{str_limit(strip_tags($newS->description), 30)}}
                                    </p>
                                    <a href="{{url('events/'.$newS->slug)}}" class="tran3s">Read More</a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

            <div class="col-md-4 theme-sidebar" style="padding: 0;">
                <div class="sidebar-recent-news">
                    <h4>Events</h4>
                    <ul>
                        @foreach($events as $event)
                            <li class="single-news clearfix">
                                <img src="@if($event->cover_photo){{ asset('images/event/'.$event->cover_photo) }}@else {{ asset('images/default/event.jpg')}} @endif" alt="" class="float-left">
                                <div class="post float-left">
                                    <h6><a href="{{url('events/'.$event->slug)}}" class="tran3s">{{$event->title}}</a></h6>
                                    <p><i class="fa fa-clock-o" aria-hidden="true"></i> {{ \Carbon\Carbon::parse($event->created_at)->format('D, M, y') }}</p>
                                </div> {{--<!-- /.post -->--}}
                            </li>
                        @endforeach
                    </ul>
                </div>
                {{--<!-- /.sidebar-recent-news -->--}}
            </div>
        </div>
    </div>
    <div class="our-blog">
        <div class="container">
            <div class="col-md-4 theme-sidebar" style="padding: 0;">
                <div class="sidebar-recent-news">
                    <h4>Top Students</h4>
                    <ul>
                        @foreach($student as $students)
                            <li class="single-news clearfix">
                                <img src="@if($students->image ){{ asset('storage/students')}}/{{ $students->image }} @else {{ asset('images/default/student.png')}} @endif" alt="" class="float-left">
                                <div class="post float-left">
                                    <h6><a class="tran3s">{{$students->name}}</a></h6>
                                    <p>
                                        {{str_limit(strip_tags($students->description), 30)}}
                                    </p>
{{--                                    <p><i class="fa fa-clock-o" aria-hidden="true"></i>  {{ \Carbon\Carbon::parse($students->created_at)->format('D, M, y') }}</p>--}}
                                </div> {{--<!-- /.post -->--}}
                            </li>
                        @endforeach
                    </ul>
                </div> {{--<!-- /.sidebar-recent-news -->--}}
            </div>
            <div class="col-md-8 theme-sidebar">
                <div class="sidebar-recent-news">
                    <h4>Feedback</h4>
                    <div class="testimonial-styleOne">
                        <div class="main-wrapper">
                            <div class="shadow"></div>
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                @foreach(\App\Testimonial::orderBy('order','asc')->get() as $k => $testimonial)
                                    <div class="item {{ $k == 0 ? 'active' : '' }}">
                                        <img class="img-responsive center" style="max-height: 200px;" src="@if($testimonial->photo ){{ asset('storage/testimonials')}}/{{ $testimonial->photo }} @else {{ asset('images/avatar.jpg')}} @endif" alt="">
                                        <h6>{{ $testimonial->writer }}</h6>
                                        <p>{{ $testimonial->comments }}</p>
                                    </div>
                                @endforeach
                            </div>

{{--                            <!-- Left and right controls -->--}}
                            <a class="left carousel-control" href="#testimonial-carousel-one" data-slide="prev">
                                <i class="fa fa-chevron-left" aria-hidden="true"></i>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#testimonial-carousel-one" data-slide="next">
                                <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="partent-logo-section">
        <div class="container">
            <div id="partner-logo">
                <div class="item"><img src="{{ asset('images/logo/vu-logo.png') }}" style="width:120px;" alt="logo"></div>
                <div class="item"><img src="{{ asset('images/logo/comsats-logo.png') }}" width="200px" alt="logo"></div>
                <div class="item"><img src="{{ asset('images/logo/pu-logo.png') }}" width="200px" alt="logo"></div>
                <div class="item"><img src="{{ asset('images/logo/bise.png') }}" width="200px" alt="logo"></div>
            </div>
        </div>
    </div>
@endsection

@section('extraScript')
    <script type="text/javascript" src="{{ asset('js/jquery.form.js')}}"></script>
<script>
    $( "#submit_request" ).click(function() {
        var phone_no = $('#contact-phone_no').val();//alert(phone);
        var maxLength =11;
        var phone_valid=phone_no.length;
        var valid=($('#contact-name').val()!='' && $('#contact-email').val()!='' && $('#contact-qualification').val()!='');
      if(!valid){
                $('#contact-name').addClass('error');
                $('#contact-email').addClass('error');
                $('#contact-phone_no').addClass('error');
                $('#contact-qualification').addClass('error');
        }else if((phone_valid<=maxLength)){
            var token = $('meta[name="_token"]').attr('content');
            var action=jQuery($(".form-validation")).attr('action');
            $.ajax({
                type: 'POST',
                url: action,
                data : jQuery($(".form-validation")).serialize(),
                headers: { 'X-XSRF-TOKEN' : token},
                success: function (data) {
                    toastr.success('Data Saved Successfully.', '', {timeOut: 5000});
                 $('#eligible-form').resetForm();
                },
                error: function (data) {
                    toastr.error('Data Not Saved.', '', {timeOut: 5000});
                },
            });
        }
    });
</script>
@endsection

