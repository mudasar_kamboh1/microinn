@extends('frontend.layouts.master')

@section('pageTitle') @lang('site.menu_home') @endsection
@section('extraStyle')
    <link type="text/css" rel="stylesheet" href="{{ asset('/frontend/rs-plugin/css/settings.css') }}"/>
@endsection
@section('pageContent')
    <!--
			=============================================
				Theme Inner Banner
			==============================================
			-->
    <!--
              =============================================
                  Theme Inner Banner
              ==============================================
              -->
    <div class="inner-banner">
        <div class="opacity">
            <div class="container">
                <h2>{{$event->title}}</h2>
            </div> <!-- /.container -->
        </div> <!-- /.opacity -->
    </div> <!-- /.inner-banner -->

    <div class="our-blog blog-innner-page blog-list blog-details">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-xs-12">
                    <div class="single-blog">
                        <div class="image"><img src="@if($event->cover_photo){{asset('images/event/'.$event->cover_photo)}}@else {{ asset('images/default/event.jpg')}} @endif" alt=""></div>
                        <h4>{{$event->title}}</h4>
                        <p>{!! $event->description !!}</p> <br>
                    </div> <!-- /.single-blog -->
                </div> <!-- /.col- -->

                <div class="col-md-4 col-sm-6 col-xs-12 theme-sidebar">
                    <div class="sidebar-categories">
                    </div> <!-- /.sidebar-categories -->
                    @include('frontend.layouts.nav-bar.partials.recent-news')
                </div> <!-- /.theme-sidebar -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div>
@endsection



