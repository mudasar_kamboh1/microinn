@extends('frontend.layouts.master')
@section('pageTitle') @lang('site.menu_home') @endsection
@section('extraStyle')
    <link type="text/css" rel="stylesheet" href="{{ asset('/frontend/rs-plugin/css/settings.css') }}"/>
@endsection
@section('pageContent')
    <div class="inner-banner">
        <div class="opacity">
            <div class="container">
                <h2>Our News</h2>

            </div> <!-- /.container -->
        </div> <!-- /.opacity -->
    </div> <!-- /.inner-banner -->


    {{--<!----}}
    {{--=============================================--}}
    {{--Our Event--}}
    {{--==============================================--}}
    {{---->--}}
    <div class="our-event event-list">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-xs-12">
                    @foreach($events as $event)
                        <div class="single-event clearfix">
                            <div class="image-box float-left"><img
                                        src="@if($event->cover_photo){{ asset('images/event/'.$event->cover_photo) }}@else {{ asset('images/default/news.jpg')}} @endif" alt=""></div>
                            <div class="text float-left">
                                <h4><a href="{{url('news/'.$event->slug)}}" class="tran3s">{{$event->title}}</a></h4>
                                <p>{{str_limit(strip_tags($event->description),200)}}</p>
                                <ul class="clearfix">
                                    <li class="float-left"><i
                                                class="flaticon-time"></i> {{ \Carbon\Carbon::parse($event->created_at)->format('D, M, y') }}
                                    </li>
                                    {{--                                <li class="float-left"><i class="flaticon-placeholder"></i> Mirpur, Dhaka</li>--}}
                                </ul>
                            </div> {{--<!-- /.text -->--}}
                        </div>
                    @endforeach

                    <ul class="theme-pagination clearfix">
                        <li>
                            <span class="tran3s">{{ $events->links() }}</span>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12 theme-sidebar">


                    @include('frontend.layouts.nav-bar.partials.recent-news')

                </div> {{--<!-- /.theme-sidebar -->--}}
            </div> {{--<!-- /.row -->--}}
        </div> {{--<!-- /.container -->--}}
    </div> {{--<!-- /.our-event -->--}}


@endsection
