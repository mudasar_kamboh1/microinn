<!DOCTYPE HTML>
<html lang="{{ app()->getLocale() }}">

<head>
    <title>{{$siteInfo['short_name']}} | @yield('pageTitle')</title>
    <link href="{{ asset('images/logo/favicon.ico') }}" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
    <meta charset="utf-8">
    <meta name="description" content="{{$siteInfo['name']}}">
    <meta name="keywords" content="school,college,management,result,exam,attendace,hostel,admission,events">
    <meta name="author" content="MicroInn">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    {{--<!-- Main style sheet -->--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
    {{--<!-- responsive style sheet -->--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css') }}">
    <link href="{{ asset('plugin/toastr/toastr.min.css')}}" rel="stylesheet">
    {{--<!--styles -->--}}

    {{--<!-- Child Page css goes here  -->--}}
    @yield("extraStyle")
    {{--<!-- Child Page css -->--}}

    {{--<!-- Locale specific font size for menu -->--}}
    @if(app()->getLocale() == 'bn')
        <style>
            .main-nav > ul > li {
                font-size: 18px;
            }
        </style>
    @endif
</head>

<body>
<div class="main-page-wrapper">

    <div id="loader-wrapper">
        <div id="loader"></div>
    </div>
    @include('frontend.partial.header')
    {{--<!-- / page header -->--}}

    {{--<!-- BEGIN CHILD PAGE-->--}}
    @yield('pageContent')
    {{--<!-- END CHILD PAGE-->--}}


    {{--<!-- footer -->--}}
    @include('frontend.partial.footer')
    {{--<!-- / footer -->--}}

    {{--<!-- Scroll Top Button -->--}}
    <button class="scroll-top tran3s">
        <i class="fa fa-angle-up" aria-hidden="true"></i>
    </button>

</div>


<script type="text/javascript" src="{{ asset('vendor/jquery.2.2.3.min.js')}}"></script>
<script type='text/javascript' src="{{ asset('vendor/Camera-master/scripts/jquery.easing.1.3.js')}}"></script>

<script type="text/javascript" src="{{ asset('vendor/bootstrap-select/dist/js/bootstrap-select.js')}}"></script>
<script type="text/javascript" src="{{ asset('vendor/bootstrap/bootstrap.min.js')}}"></script>
<script type='text/javascript'
        src="{{ asset('vendor/Camera-master/scripts/jquery.mobile.customized.min.js')}}"></script>


<script type='text/javascript' src="{{ asset('vendor/Camera-master/scripts/camera.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('vendor/bootstrap-mega-menu/js/menu.js')}}"></script>
<script type="text/javascript" src="{{ asset('vendor/WOW-master/dist/wow.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('vendor/owl-carousel/owl.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('vendor/Counter/jquery.counterup.min.js')}}"></script>
<script src="{{ asset('vendor/Counter/jquery.waypoints.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('vendor/particles.js-master/particles.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('vendor/particles.js-master/demo/js/lib/stats.js')}}"></script>
<script type="text/javascript" src="{{ asset('vendor/particles.js-master/demo/js/app.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/theme.js')}}"></script>
<!-- Validation -->
<script type="text/javascript" src="{{ asset('js/jquery.form.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/theme-contact-form.js')}}"></script>
{{--<script type="text/javascript" src="{{ asset('js/validate.js')}}"></script>--}}
<script type="text/javascript" src="{{ asset('plugin/toastr/toastr.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('plugin/sweet-alert/sweetalert2@8.js')}}"></script>
@yield("extraScript")
</body>
</html>
