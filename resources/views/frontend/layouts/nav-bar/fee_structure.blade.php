@extends('frontend.layouts.master')

@section('pageTitle') @lang('site.menu_home') @endsection
@section('extraStyle')
    <link type="text/css" rel="stylesheet" href="{{ asset('/frontend/rs-plugin/css/settings.css') }}"/>
@endsection

@section('pageContent')
    {{-- <!--=============================================
            Theme Inner Banner
        ==============================================
        -->--}}
    <div class="inner-banner">
        <div class="opacity">
            <div class="container">
                <h2>FEE STRUCTURE</h2>

            </div>
        </div>
    </div>

    <div class="container contact-us-page">
        @if(!session()->has('from_submitted'))
            <div class="row">
                <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 wow fadeInLeft">
                    <div class="card">
                        <div class="contact-us-form text-center card-body">
                            <h5>Please fill this form to continue!</h5>
                            <hr>
                            <form class="form-validation" method="post" action="{{url('/post-suitability')}}"
                                  id="eligible-form">
                                <meta name="_token" content="{{ csrf_token() }}"/>
                                {{ csrf_field() }}

                                <input type="hidden" name="route" value="{{ encrypt(request()->segment(1)) }}">

                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="single-input">
                                            <input type="text" placeholder="You Name*" id="contact-name" name="name">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="single-input">
                                            <input type="email" placeholder="Enter Email here*" id="contact-email"
                                                   name="email">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="single-input">
                                            <input type="text" placeholder="Phone Number" id="contact-phone_no"
                                                   name="phone_no">
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="single-input">
                                            <input type="text" placeholder="Qualification*" id="contact-qualification"
                                                   name="qualification">
                                        </div>
                                    </div>
                                </div>
                                <input type="submit" value="Next" id="eligibilty" class="tran3s p-bg-color">
                            </form>
                            {{--<!-- Contact Form Validation Markup -->
                                                <!-- Contact alert -->--}}
                            <div class="alert-wrapper" id="alert-success">
                                <div id="success">
                                    <button class="closeAlert"><i class="fa fa-times" aria-hidden="true"></i></button>
                                    <div class="wrapper">
                                        <p>Your message was sent successfully.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="alert-wrapper" id="alert-error">
                                <div id="error">
                                    <button class="closeAlert"><i class="fa fa-times" aria-hidden="true"></i></button>
                                    <div class="wrapper">
                                        <p>Sorry!Something Went Wrong.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div style="overflow: hidden; margin: 15px auto;width: 100%; {{ !session()->has('from_submitted') ? 'display:none' : 'display:block' }}">
            <iframe scrolling="no" src="http://www.vu.edu.pk/FeeStructure_Local.aspx" style="border: 0px none;  height: 1800px;margin-bottom: -90px; margin-top: -250px; width:100%;">
            </iframe>
        </div>
    </div>

    <br>
    <br>

@endsection

@section('extraScript')
    <script type="text/javascript" src="{{ asset('js/validate.js')}}"></script>
@endsection
