@extends('frontend.layouts.master')

@section('pageTitle') @lang('site.menu_home') @endsection
@section('extraStyle')
    <link type="text/css" rel="stylesheet" href="{{ asset('/frontend/rs-plugin/css/settings.css') }}" />
@endsection

@section('pageContent')
    <div class="inner-banner">
        <div class="opacity">
            <div class="container">
                <h2>About Us</h2>
            </div>
        </div>
    </div>

    <div class="about-text">
        <div class="container">
            <div class="wrapper">
                <div class="row">
                    <div class="col-sm-6 col-xs-12">
                        <img src="images/inner-page/1.jpg" alt="" class="space">
                        <div class="theme-title">
                            <h2>{{$about->key_point_2_title}}</h2>
                        </div>
                        <p>{!! $about->key_point_2_content !!}</p>
                    </div>
                    <div class="col-sm-6 col-xs-12 m-fix">
                        <div class="theme-title">
                            {{--<h6>{{$about->why_content}}</h6>--}}
                            <h2>{{$about->key_point_1_title}}</h2>
                        </div>
                        <p class="space">{!! $about->key_point_1_content!!}</p>
                        <img src="images/inner-page/2.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <br>
@endsection


