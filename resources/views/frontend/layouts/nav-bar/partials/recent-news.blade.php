<div class="sidebar-recent-news">
    <h4>Recent News</h4>
    <ul>
        @foreach($news as $newS)
        <li class="single-news clearfix">
            <img src="@if($event->cover_photo){{ asset('images/event/'.$newS->cover_photo) }}@else {{ asset('images/default/news.jpg')}} @endif" alt="" class="float-left">
            <div class="post float-left">
                <h6><a href="{{url('news/'.$newS->slug)}}" class="tran3s">{{$newS->title}}</a></h6>
                <p><i class="fa fa-clock-o" aria-hidden="true"></i> {{ \Carbon\Carbon::parse($event->created_at)->format('D, M, y') }}</p>
            </div>
        </li>
            @endforeach
    </ul>
</div>
