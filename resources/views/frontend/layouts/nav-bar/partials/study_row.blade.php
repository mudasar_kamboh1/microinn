<select name="study" class="form-control studyProgram" id="studyProgram">
    <option value="">Study Program</option>
    @foreach($degree as $institute)
        <option value="{{$institute->id}}">{{$institute->title}}</option>
    @endforeach
</select>
