<select name="degree" class="form-control degreeProgram" id="degreePrograms">
    <option value="">Degree Program</option>
@foreach($degree as $institute)
        <option value="{{$institute->id}}">{{$institute->title}}</option>
    @endforeach

</select>
@section('extraScript')
    <script type="text/javascript" src="{{ asset('js/validate.js')}}"></script>
    <script>
        $('#institute_selector').change(function () {
            var selectedInstitute = $(this).children("option:selected").val();
            $.ajax({
                url: '/degree/' + selectedInstitute + '/degreeProgram',
                method: 'GET',
                type: 'JSON',
                success: function (response) {
                    $(".degreeProgram").replaceWith(response.view);
                }
            })
        });
        $(document).on('change','#degreePrograms',function(){
    // $('#degreePrograms').change(function () {
        var selectedInstitute = $(this).children("option:selected").val();
        $.ajax({
            url: '/degree/' + selectedInstitute+'/studyProgram',
            method: 'GET',
            type: 'JSON',
            success: function (response) {
                $(".studyProgram").replaceWith(response.view);
            }
        })
    });
</script>
@endsection
