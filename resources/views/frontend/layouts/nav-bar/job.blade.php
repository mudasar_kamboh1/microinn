@extends('frontend.layouts.master')
@section('pageTitle') @lang('site.menu_home') @endsection
@section('extraStyle')
    <link type="text/css" rel="stylesheet" href="{{ asset('/frontend/rs-plugin/css/settings.css') }}"/>
    <style>
        /* .jobs-lisiting {
             width: 158%;
             padding: 40px 20px 16px 40px;
             border-top: 1px solid #e9e9e9;
             border-left: none;
             border-radius: 0 3px 3px 0;
         }*/
        .job-button {
            width: 200px;
            color: #fff !important;
            text-align: center;
            font-family: 'Montserrat', sans-serif;
            font-weight: 700;
            text-transform: uppercase;
            background: #303030;
            line-height: 60px;
            border-radius: 3px;
        }

        /*.our-event.event-list .single-event .text*/
        .jobs-lisiting {
            /*width: 58%; */
            padding: 40px 20px 16px 40px;
            border-top: 1px solid #e9e9e9;
            border-left: none;
            border-radius: 0 3px 3px 0;
        }
    </style>
@endsection
@section('pageContent')
    <div class="inner-banner">
        <div class="opacity">
            <div class="container">
                <h2>Jobs</h2>

            </div> {{--<!-- /.container -->--}}
        </div> {{--<!-- /.opacity -->--}}
    </div> {{--<!-- /.inner-banner -->--}}



    <div class="our-event event-list">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    @foreach($jobs as $job)
                        <div class="single-event clearfix">
                            <div class="text" style="width:100%;">
                                <h2 class="tran3s">{{$job->title}}</h2>
                                <p>{{$job->eligibility}}</p>
                                <ul class="clearfix">
                                    <li class="float-left"><i class="flaticon-time"></i> Open Date: <b>{{ \Carbon\Carbon::parse($job->open_date)->format('D, M, y') }}</b></li>
                                    <li class="float-left"><i class="flaticon-time"></i>  Close Date <b>{{ \Carbon\Carbon::parse($job->close_date)->format('D, M, y') }}</b></li>
                                </ul>
                                <span><a class="job-button"
                                         href="{{url('/jobRequest/'.encrypt($job->id))}}">Send Request!</a></span>

                            </div> {{--<!-- /.text -->--}}
                        </div>
                    @endforeach
                    <ul class="theme-pagination clearfix">
                        <li>
                            <span class="tran3s">{{ $jobs->links() }}</span>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12 theme-sidebar">
                </div> {{--<!-- /.theme-sidebar -->--}}
            </div> {{--<!-- /.row -->--}}
        </div> {{--<!-- /.container -->--}}
    </div> {{--<!-- /.our-event -->--}}


@endsection
