@extends('frontend.layouts.master')

@section('pageTitle') @lang('site.menu_home') @endsection
@section('extraStyle')
    <link type="text/css" rel="stylesheet" href="{{ asset('/frontend/rs-plugin/css/settings.css') }}"/>
{{--    <link href="{{ asset('/plugin/date-picker/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" media="screen">--}}
    <link href="{{ asset('/plugin/date-picker/bootstrap/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" media="screen">
@endsection
<style>
    select{
        height: 50px !important;
    }
</style>
@section('pageContent')

    <div class="inner-banner">
        <div class="opacity">
            <div class="container">
                <h2>Job Request</h2>
            </div> <!-- /.container -->
        </div> <!-- /.opacity -->
    </div> <!-- /.inner-banner -->


    <div class="container contact-us-page">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 wow fadeInLeft">
                <div class="cards">
                    <div class="contact-us-form text-center card-body">
                        <h5>Please fill this form!</h5>
                        <hr>
                        <form class="form-validation" method="post" action="{{url('/post-inquiry-request')}}" id="eligible-form" enctype="multipart/form-data">
                            <meta name="_token" content="{!! csrf_token() !!}"/>
                            {{ csrf_field() }}
                            <input type="hidden" name="job_id" value="{{ encrypt(request()->segment(1)) }}">

                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="single-input">
                                        <input type="text" placeholder="Job Title" id="job_title" name="job_title">
                                    </div> {{--<!-- /.single-input -->--}}
                                </div>{{-- <!-- /.col- -->--}}
                                <div class="col-sm-6 col-xs-12">
                                    <div class="single-input">
                                        <select name="job_locator" id="job_locator" class="form-control" required>
                                            <option selected="selected" value="">Select</option>
                                            <option value="Sheikhupura City.">Sheikhupura City.</option>
                                            <option value="Shahdara, Lahore.">Shahdara, Lahore.</option></select>
                                    </div> {{--<!-- /.single-input -->--}}
                                </div> {{--<!-- /.col- -->--}}
                                <div class="col-sm-6 col-xs-12">
                                    <div class="single-input">
                                        <input type="text" placeholder="Name" id="name"
                                               name="name">
                                    </div> {{--<!-- /.single-input -->--}}
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="single-input">
                                        <input type="text" placeholder="CNIC" id="cnic"
                                               name="cnic">
                                    </div> {{--<!-- /.single-input -->--}}
                                </div> {{--<!-- /.col- -->--}}
                                <div class="col-sm-6 col-xs-12">
                                    <div class="single-input">
                                        <div class="form-group">
                                            <div aria-placeholder="test" class="input-group date form_date col-md-12" data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                                                <input placeholder="date of birth" class="form-control" size="16" type="text" value="" readonly>
                                                <!--                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>-->
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                            </div>
                                            <input type="hidden" id="dtp_input2" value="" name="dob" /><br/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="single-input">
                                        <select name="gender" class="form-control" id="gender">
                                            <option value="">Gender</option>
                                            <option value="0">Female</option>
                                            <option value="1">Male</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="single-input">
                                        <select name="martial_status" class="form-control" id="martial_status">
                                            <option value="">Marital Status</option>
                                            <option value="0">Single</option>
                                            <option value="1">Double</option>
                                        </select>
                                    </div>
                                </div>
                                <h5>Contact Information</h5>
                                <hr>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="single-input">
                                        <input type="text" placeholder="Cell No" id="cell"
                                               name="cell">

                                    </div>

                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="single-input">
                                        <input type="text" placeholder="Email" id="email"
                                               name="email">
                                    </div>

                                </div>
                                <div class="col-xs-12">
                                    <div class="single-input">
                                        <input type="text" placeholder="Residence" id="residence" name="residence">
                                    </div> {{--<!-- /.single-input -->--}}
                                </div>
                                <h5>Qualification</h5><hr>
                                <div class="col-xs-12">
                                    <div class="single-input">
                                        <input type="text" placeholder="Educational" id="educational" name="educational">
                                    </div> {{--<!-- /.single-input -->--}}
                                </div>
                                <div class="col-xs-12">
                                    <div class="single-input">
                                        <input type="text" placeholder="Professional" id="professional" name="professional">
                                    </div> {{--<!-- /.single-input -->--}}
                                </div>
                                <div class="col-xs-12">
                                    <div class="single-input">
                                        <input type="text" placeholder="Experience" id="experience" name="experience">
                                    </div> {{--<!-- /.single-input -->--}}
                                </div>

                                <div class="col-xs-12">
                                    <div class="single-input">
                                        <input type="text" placeholder="Reference" id="reference" name="reference">
                                    </div> {{--<!-- /.single-input -->--}}
                                </div>

                                <div class="col-xs-12">
                                    <div class="single-input">
    {{--
                                        <input  type="file"  name="image">
    --}}
                                        <input type="file" name="photo" id="photo">

                                    </div> {{--<!-- /.single-input -->--}}
    {{--                                accept=".jpeg, .jpg, .png"--}}
                                </div>
                            </div>

                            <input type="submit" value="Submit" id="eligibilty" class="tran3s p-bg-color">

                        </form>

{{--                        <!-- Contact Form Validation Markup -->--}}
{{--                        <!-- Contact alert -->--}}
                        <div class="alert-wrapper" id="alert-success">
                            <div id="success">
                                <button class="closeAlert"><i class="fa fa-times" aria-hidden="true"></i></button>
                                <div class="wrapper">
                                    <p>Your request is successfully submitted.</p>
                                </div>
                            </div>
                        </div> {{--<!-- End of .alert_wrapper -->--}}
                        <div class="alert-wrapper" id="alert-error">
                            <div id="error">
                                <button class="closeAlert"><i class="fa fa-times" aria-hidden="true"></i></button>
                                <div class="wrapper">
                                    <p>Sorry!Something Went Wrong.</p>
                                </div>
                            </div>
                        </div> {{--<!-- End of .alert_wrapper -->--}}
                    </div> {{--<!-- /.contact-us-form -->--}}
                </div>
            </div>
        </div>{{-- <!-- /.row -->--}}
    </div>{{-- <!-- /.container -->--}}
{{--

    <br>
    <br>
--}}

@endsection
@section('extraScript')
    <script type="text/javascript" src="{{ asset('js/validate.js')}}"></script>
{{--    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker();
        });
    </script>--}}
{{--    <script type="text/javascript" src="{{ asset('/plugin/date-picker/bootstrap/js/bootstrap.min.js')}}"></script>--}}
    <script type="text/javascript" src="{{ asset('/plugin/date-picker/bootstrap/js/bootstrap-datetimepicker.js')}}" charset="UTF-8"></script>
    <script type="text/javascript" src="{{ asset('/plugin/date-picker/bootstrap//js/locales/bootstrap-datetimepicker.fr.js')}}" charset="UTF-8"></script>
    <script type="text/javascript">
        $('.form_date').datetimepicker({
            language:  'en',
            weekStart: 1,
            todayBtn:  1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0
        });
    </script>
@endsection


