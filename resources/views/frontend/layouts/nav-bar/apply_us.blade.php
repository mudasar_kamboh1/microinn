
@extends('frontend.layouts.master')

@section('pageTitle') @lang('site.menu_home') @endsection
@section('extraStyle')
    <link type="text/css" rel="stylesheet" href="{{ asset('/frontend/rs-plugin/css/settings.css') }}"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
@endsection

@section('pageContent')
    <style>
        select{
            height:50px !important;
            border:1px solid #F1F1ED !important;
        }
    </style>

    <div class="inner-banner">
        <div class="opacity">
            <div class="container">
                <h2>Apply Online</h2>
            </div>
        </div>
    </div>

    <div class="container contact-us-page">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 wow fadeInLeft">
                <div class="card">
                    <div class="contact-us-form text-center card-body">
                        <h5>Please fill this form to continue!</h5>
                        <hr>
                        <form class="form-validation" method="post" action="{{url('/post-inquiry')}}"
                              id="eligible-form">
                            <meta name="_token" content="{!! csrf_token() !!}"/>
                            {{ csrf_field() }}

                            <input type="hidden" name="route" value="{{ encrypt(request()->segment(1)) }}">
                            <div class="row">


                                {{--  <div class="col-sm-6 col-xs-12">
                                      --}}{{--                                <div class="single-input">--}}{{--
                                      --}}{{--                                    <input type="text" id="datepicker" name="inquiry_date" placeholder="Inquiry Date">--}}{{--
                                      <div aria-placeholder="test" class="input-group date form_date col-md-12"
                                           data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2"
                                           data-link-format="yyyy-mm-dd">
                                          <input placeholder="Inquiry Date" size="16" type="text" value="" readonly>
                                          <!--                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>-->
                                          <span class="input-group-addon"><span
                                                      class="glyphicon glyphicon-calendar"></span></span>
                                      </div>
                                      --}}{{--                                    <input type="text" id="datepicker" name="inquiry_date" placeholder="Inquiry Date">--}}{{--
                                      <input type="hidden" id="dtp_input2" name="inquiry_date"/><br/>
                                      --}}{{--                                </div>--}}{{--
                                  </div>--}}
                                {{-- <div class="col-sm-6 col-xs-12">
                                     <div class="single-input">
                                         <input type="text" placeholder="Inquiry no.*" id="inquiry_no" name="inquiry_no" value="{{str_random(15)}}">

                                     </div>
                                 </div>--}}
                                <div class="col-xs-12">
                                    <div class="single-input">
                                        <input type="text" placeholder="Name" id="name" name="name">
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="single-input">
                                        <select name="gender" class="form-control">
                                            <option value="">Gender</option>
                                            <option value="0">Female</option>
                                            <option value="1">Male</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="single-input">
                                        <input type="text" placeholder="Contact" id="contact" name="contact">
                                    </div>
                                </div>

                                <div class="col-sm-6 col-xs-12">
                                    <div class="single-input">
                                        <input type="text" placeholder="Location" id="town" name="town">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="single-input">
                                        <select name="qualification" id="qualification" class="form-control">
                                            <option value="">Select Qualification</option>
                                            <option value="Matric /O Level">Matric /O Level</option>
                                            <option value="Intermediate">Intermediate</option>
                                            <option value="Bachelors">Bachelors</option>
                                            <option value="Masters">Masters</option>
                                            <option value="Other Education">Other Education</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="single-input">
                                        <input type="text" placeholder="Total Marks" id="total_marks"
                                               name="total_marks">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="single-input">
                                        <select name="institute" class="form-control" id="institute_selector">
                                            <option value="">Study Institute</option>
                                            @foreach($institutes as $institute)
                                                <option value="{{$institute->id}}">{{$institute->institute}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="single-input">
                                        @include('frontend.layouts.nav-bar.partials.degree_row')
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="single-input">
                                        @include('frontend.layouts.nav-bar.partials.study_row')
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="single-input">
                                        <select name="availability" id="availability" class="form-control">
                                            <option value="">Select Availability</option>
                                            <option value="Anytime">Anytime</option>
                                            <option value="Morning">Morning</option>
                                            <option value="Mid Day">Mid Day</option>
                                            <option value="Evening">Evening</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="single-input">
                                        <select name="info_source" id="info_source" class="form-control">
                                            <option value="">Select Info Source</option>
                                            <option value="Print Media">Print Media</option>
                                            <option value="Electronic Media">Electronic Media</option>
                                            <option value="Word of Mouth">Word of Mouth</option>
                                            <option value="Web/Search Engine">Web/Search Engine</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <input type="submit" value="Submit" id="eligibilty" class="tran3s p-bg-color">
                                </div>
                            </div>
                        </form>
                        <div class="alert-wrapper" id="alert-success">
                            <div id="success">
                                <button class="closeAlert"><i class="fa fa-times" aria-hidden="true"></i></button>
                                <div class="wrapper">
                                    <p>Your Query Saved successfully.</p>
                                </div>
                            </div>
                        </div>
                        <div class="alert-wrapper" id="alert-error">
                            <div id="error">
                                <button class="closeAlert"><i class="fa fa-times" aria-hidden="true"></i></button>
                                <div class="wrapper">
                                    <p>Sorry! Something Went Wrong.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <br>
@endsection
@section('extraScript')
    {{--    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>--}}
    <script type="text/javascript" src="{{ asset('js/validate.js')}}"></script>
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker();
        });
    </script>
    <script type="text/javascript" src="{{ asset('/plugin/date-picker/bootstrap/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('/plugin/date-picker/bootstrap/js/bootstrap-datetimepicker.js')}}"
            charset="UTF-8"></script>
    <script type="text/javascript"
            src="{{ asset('/plugin/date-picker/bootstrap//js/locales/bootstrap-datetimepicker.fr.js')}}"
            charset="UTF-8"></script>
    <script type="text/javascript">
        $('.form_date').datetimepicker({
            language: 'en',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0
        });
    </script>
    <script>

    </script>

@endsection
