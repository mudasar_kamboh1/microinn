<!-- Master page  -->
@extends('backend.layouts.master')

{{--<!-- Page title -->--}}
@section('pageTitle') Events @endsection
{{--<!-- End block -->--}}

{{--<!-- Page body extra class -->--}}
@section('bodyCssClass') @endsection
{{--<!-- End block -->--}}

{{--<!-- BEGIN PAGE CONTENT-->--}}
@section('pageContent')
    {{--    <!-- Section header -->--}}
    <section class="content-header">
        <h1>
            Contact Us
            <small>List</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{URL::route('site.dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#"><i class="fa icon-teacher"></i> Contact Us</a></li>

        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title">Contact Us</h3>

                        {{--<div class="box-tools pull-right">
                            <a class="btn btn-info btn-sm" href="{{ URL::route('jobs.create') }}"><i class="fa fa-plus-circle"></i> Add New</a>
                        </div>--}}
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="eventList" class="table table-bordered table-striped list_view_table">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Message</th>
                                {{--                                <th width="30%">cnic</th>--}}
{{--                                <th width="150px">Action</th>--}}
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($contacts as $contact)
                                <tr>
                                    <td> {{ $contact->name }}</td>
                                    <td> {{ $contact->email }}</td>
                                    <td> {{ $contact->message }}</td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        {{ $contacts->links() }}
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>

    </section>
    <!-- /.content -->
@endsection
<!-- END PAGE CONTENT-->

<!-- BEGIN PAGE JS-->
@section('extraScript')
    <script type="text/javascript">
        $(document).ready(function () {
            Site.EventInit();
            initDeleteDialog();
        });
    </script>
@endsection
<!-- END PAGE JS-->
