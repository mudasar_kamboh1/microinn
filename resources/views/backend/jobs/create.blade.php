<!-- Master page  -->
@extends('backend.layouts.master')

<!-- Page title -->
@section('pageTitle') Jobs @endsection
<!-- End block -->

<!-- Page body extra class -->
@section('bodyCssClass') @endsection
<!-- End block -->

<!-- BEGIN PAGE CONTENT-->
@section('pageContent')
    <!-- Section header -->
    <section class="content-header">
        <h1>
            Jobs
            {{--            <small>@if($teacher) Update @else Add New @endif</small>--}}
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{URL::route('user.dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{URL::route('jobs.index')}}"><i class="fa icon-teacher"></i> Jobs</a></li>
            {{--            <li class="active">@if($teacher) Update @else Add @endif</li>--}}
        </ol>
    </section>
    <!-- ./Section header -->
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <form novalidate id="entryForm" action="@if($job){{URL::Route('jobs.update', $job->id)}} @else {{URL::Route('jobs.store')}}  @endif" method="post" enctype="multipart/form-data">
                        <div class="box-header">
                            <h3 class="box-title">@if($job) Update @else Add @endif Event<span class="text-danger"> * Marks are required feild</span></h3>
                        </div>
                        <div class="box-body">
                            @csrf
                              @if($job)  {{ method_field('PATCH') }} @endif
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="name">Title<span class="text-danger">*</span></label>
                                        <input autofocus type="text" class="form-control" name="title" placeholder="name" value="@if($job){{ $job->title }}@else{{ old('title') }} @endif" required minlength="2" maxlength="255">
                                        <span class="fa fa-info form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('title') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="dob">Open Date<span class="text-danger">*</span></label>
                                        <input type='text' class="form-control date_time_picker"  readonly name="open_date" placeholder="open date" value="@if($job){{ $job->open_date->format('d/m/Y h:i a') }}@else{{ old('open_date',date('d/m/Y h:i a')) }} @endif" required minlength="10" maxlength="255" />
                                        <span class="fa fa-calendar form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('open_date') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="dob">Close Date<span class="text-danger">*</span></label>
                                        <input type='text' class="form-control date_picker2"  readonly name="close_date" placeholder="close date" value="@if($job){{ $job->close_date->format('d/m/Y h:i a') }}@else{{ old('close_date',date('d/m/Y h:i a')) }} @endif" required minlength="10" maxlength="255" />
                                        <span class="fa fa-calendar form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('close_date') }}</span>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group has-feedback">
                                    <label for="address">Eligibility</label>
                                    <textarea name="eligibility" class="form-control"  maxlength="500" >@if($job){{ $job->eligibility }}@else{{ old('eligibility') }} @endif</textarea>
                                    {{--<span class="fa fa-location-arrow form-control-feedback"></span>--}}
                                    <span class="text-danger">{{ $errors->first('eligibility') }}</span>
                                </div>
                            </div>
                        </div>
                        <hr>
                <div class="box-footer">
                    <a href="{{URL::route('jobs.index')}}" class="btn btn-default">Cancel</a>
                    <button type="submit" class="btn btn-info pull-right"><i class="fa fa-plus-circle"></i>  Add </button>

                </div>
                </form>
            </div>
        </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
<!-- END PAGE CONTENT-->

<!-- BEGIN PAGE JS-->
@section('extraScript')
    <script type="text/javascript">
        $(document).ready(function () {
            Generic.initCommonPageJS();
        });
    </script>
@endsection
