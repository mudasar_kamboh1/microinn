<!-- Master page  -->
@extends('backend.layouts.master')

{{--<!-- Page title -->--}}
@section('pageTitle') Events @endsection
{{--<!-- End block -->--}}

{{--<!-- Page body extra class -->--}}
@section('bodyCssClass') @endsection
{{--<!-- End block -->--}}

{{--<!-- BEGIN PAGE CONTENT-->--}}
@section('pageContent')
{{--    <!-- Section header -->--}}
    <section class="content-header">
        <h1>
            Jobs
            <small>List</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{URL::route('site.dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#"><i class="fa icon-teacher"></i> Jobs</a></li>

        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title">Jobs</h3>

                        <div class="box-tools pull-right">
                            <a class="btn btn-info btn-sm" href="{{ URL::route('jobs.create') }}"><i class="fa fa-plus-circle"></i> Add New</a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="eventList" class="table table-bordered table-striped list_view_table">
                            <thead>
                            <tr>
                                <th width="40%">Title</th>
                                <th width="20%">Open Date</th>
                                <th width="30%">Close Date</th>
                                <th width="30%">Eligibility</th>
                                <th width="10%">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($jobs as $job)
                                <tr>

                                    <td>{{ $job->title }}</td>
                                    <td> {{ $job->open_date->format('d/m/Y') }}</td>
                                    <td> {{ $job->close_date->format('d/m/Y') }}</td>
                                    <td> {{ $job->eligibility }}</td>
                                </tr>
                            @endforeach

                            </tbody>
                            <tfoot>
                            <tr>
                                <th width="40%">Title</th>
                                <th width="20%">Open Date</th>
                                <th width="30%">Close Date</th>
                                <th width="30%">Eligibility</th>
                                <th width="10%">Action</th>
                            </tr>
                            </tfoot>
                        </table>
                        {{ $jobs->links() }}
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>

    </section>
    <!-- /.content -->
@endsection
<!-- END PAGE CONTENT-->

<!-- BEGIN PAGE JS-->
@section('extraScript')
    <script type="text/javascript">
        $(document).ready(function () {
            Site.EventInit();
            // initDeleteDialog();
            Generic.EventInit();
        });
    </script>
@endsection
<!-- END PAGE JS-->
