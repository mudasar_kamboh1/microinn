<!-- Master page  -->
@extends('backend.layouts.master')

<!-- Page title -->
@section('pageTitle') Student Certificate @endsection
<!-- End block -->

<!-- Page body extra class -->
@section('bodyCssClass') @endsection
<!-- End block -->

<!-- BEGIN PAGE CONTENT-->
@section('pageContent')
    <!-- Section header -->
    <section class="content-header">
        <h1>
            Student Certificate
            <small>List</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{URL::route('user.dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Student certificate</li>
        </ol>
    </section>
    <!-- ./Section header -->
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                        <table id="listDataTableWithSearch" class="table table-bordered table-striped list_view_table display responsive no-wrap" width="100%">
                            <thead>
                            <tr>
                                <th width="5%">#</th>
                                <th class="notexport" width="7%">Photo</th>
                                <th width="8%">Regi. No.</th>
                                <th width="19%">Name</th>
                                <th width="10%">Phone No</th>
                                <th width="10%">Email</th>
                                <th width="10%">Certificate</th>
                                <th class="notexport" width="15%">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($students as $student)
                                <tr>
                                    <td>
                                        {{$loop->iteration}}
                                    </td>
                                    <td>
                                        <img class="img-responsive center" style="height: 35px; width: 35px;" src="@if($student->photo ){{ asset('uploads/students')}}/{{ $student->photo }} @else {{ asset('images/avatar.jpg')}} @endif" alt="">
                                    </td>
                                    <td>{{ $student->reg_no }}</td>
                                    <td>{{ $student->name }}</td>
                                    <td>{{ $student->phone_no }}</td>
                                    <td>{{ $student->email }}</td>
                                    <td>
                                        <!-- todo: have problem in mobile device -->
                                        @role('Admin')
                                            @if($student->certificate_status == 2)
                                                <span class="label label-success">Delivered</span>
                                            @else($student->certificate_status == 1)
                                                <input class="toggle-event toggle-event1" type="checkbox" data-pk="{{$student->id}}" @if($student->certificate_status == 1) checked @endif data-toggle="toggle" data-on="Issued" data-off="Not Issue" data-onstyle="info" data-offstyle="warning">
                                            @endif
                                        @else
                                            @if($student->certificate_status == 0)
                                                <span class="label label-info">Pending</span>
                                            @else
                                                @if($student->certificate_status == 1)
                                                    <input class="toggle-event toggle-event2" type="checkbox" data-pk="{{$student->id}}" @if($student->certificate_status == 2) checked disabled @endif data-toggle="toggle" data-on="Delivered" data-off="Issued" data-onstyle="info" data-offstyle="warning">
                                                @elseif($student->certificate_status == 2)
                                                    <span class="label label-success">Delivered</span>
                                                @endif
                                            @endif
                                        @endrole
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <a title="Details"  href="{{URL::route('student.show',$student->id)}}"  class="btn btn-primary btn-sm"><i class="fa fa-eye"></i>
                                            </a>
                                        </div>
                                        <div class="btn-group">
                                            <a title="Edit" href="{{URL::route('student.edit',$student->id)}}" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></a>
                                            </a>
                                        </div>
                                        <!-- todo: have problem in mobile device -->
                                        <div class="btn-group">
                                            <form  class="myAction" method="POST" action="{{URL::route('student.destroy', $student->id)}}">
                                                @csrf
                                                <input name="_method" type="hidden" value="DELETE">
                                                <button type="submit" class="btn btn-danger btn-sm" title="Delete">
                                                    <i class="fa fa-fw fa-trash"></i>
                                                </button>
                                            </form>
                                        </div>

                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>

    </section>
    <!-- /.content -->

    <div id="deliver-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <form action="{{route('deliverCertificate')}}" method="POST">
                @csrf
                <input type="hidden" name="student_id" class="student_pk_id" >
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Detial</h4>
                    </div>

                    <div class="modal-body">
                        <div class="form-group">
                            <label>Receive By</label>
                            <input type="text" name="receive_by" placeholder="Receive By" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Receive Phone</label>
                            <input type="text" name="receive_phone" placeholder="Receive By" class="form-control" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-info">Deliver</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
<!-- END PAGE CONTENT-->

<!-- BEGIN PAGE JS-->
@section('extraScript')
    <script type="text/javascript">
        $(document).ready(function () {
            window.postUrl = '{{URL::Route("student.certificateStatus", 0)}}';
            window.section_list_url = '{{URL::Route("academic.section")}}';
            window.changeExportColumnIndex = 7;
            window.excludeFilterComlumns = [0,1,8,9];
           Academic.studentInit();
           $('title').text($('title').text() + '-' + $('select[name="class_id"] option[selected]').text() + '(' + $('select[name="section_id"] option[selected]').text() +')');

            $('.toggle-event2').change(function() {
                var student_id = $(this).data('pk');
                $('.student_pk_id').val(student_id);
                $('#deliver-modal').modal('show');
                return false;
            });

            $('.toggle-event1').change(function() {
                var student_id = $(this).data('pk');
                $.ajax({
                    url: '{{url("student/certificate-status")}}/'+student_id,
                    type:'GET',
                    success: function(result){
                    if(result.success){
                        toastr.success(result.message);
                    }else{
                        toastr.danger(result.message);
                    }
                }});
            });

            $('.toggle-event2').change(function() {
                var student_id = $(this).data('pk');
                $.ajax({
                    url: '{{url("student/certificate-status1")}}/'+student_id,
                    type:'GET',
                    success: function(result){
                        if(result.success){
                            toastr.success(result.message);
                        }else{
                            toastr.danger(result.message);
                        }
                    }});
            });

        })
    </script>
@endsection
<!-- END PAGE JS-->
