<!-- Master page  -->
@extends('backend.layouts.master')

<!-- Page title -->
@section('pageTitle') Study Complete @endsection
<!-- End block -->

<!-- Page body extra class -->
@section('bodyCssClass') @endsection
<!-- End block -->

<!-- BEGIN PAGE CONTENT-->
@section('pageContent')
    <!-- Section header -->
    <section class="content-header">
        <h1>
            Student Complete
            <small>List</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{URL::route('user.dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Student certificate</li>
        </ol>
    </section>
    <!-- ./Section header -->
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="listDataTableWithSearch" class="table table-bordered table-striped list_view_table display responsive no-wrap" width="100%">
                                <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th class="notexport" width="7%">Photo</th>
                                    <th width="8%">Regi. No.</th>
                                    <th width="19%">Name</th>
                                    <th width="10%">Phone No</th>
                                    <th width="10%">Email</th>
                                    <th class="notexport" width="15%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($students as $student)
                                    <tr>
                                        <td>
                                            {{$loop->iteration}}
                                        </td>
                                        <td>
                                            <img class="img-responsive center" style="height: 35px; width: 35px;" src="@if($student->photo ){{ asset('uploads/students')}}/{{ $student->photo }} @else {{ asset('images/avatar.jpg')}} @endif" alt="">
                                        </td>
                                        <td>{{ $student->reg_no }}</td>
                                        <td>{{ $student->name }}</td>
                                        <td>{{ $student->phone_no }}</td>
                                        <td>{{ $student->email }}</td>
                                        <td>
                                            @if(is_null($student->coursesFee()->first()) || !is_null($student->coursesFee()->first()) && $student->coursesFee()->first()->passout_status != 1)
                                                <div class="btn-group">
                                                    <button type="button" data-toggle="modal" onclick="$('.student_pk_id').val({{$student->id}})" data-target="#passout-modal" title="Passout this student" class="open-AddBookDialog btn btn-info btn-md">Passout</button>
                                                </div>
                                            @else
                                                <span class="label label-success">Passout Done</span>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
        <div id="passout-modal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <form action="{{route('passOut')}}" method="POST">
                    @csrf
                    <input type="hidden" name="student_id" class="student_pk_id" id="{{$student->id}}" >
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Passout</h4>
                    </div>

                    <div class="modal-body">
                        {{--<div class="form-group">--}}
                            {{--<label>Course</label>--}}
                            {{--<select name="course_id" class="form-control select2 course">--}}
                                {{--<option value="">Select Course</option>--}}
                                {{--@foreach(\App\Course::get() as $course)--}}
                                    {{--<option value="{{$course->id}}" >{{$course->name}} | {{ !is_null($course->campus) ? $course->campus->name : '' }}</option>--}}
                                {{--@endforeach--}}
                            {{--</select>--}}
                        {{--</div>--}}
                        <div class="form-group">
                            <label>Attendance %</label>
                            <input type="number" min="0" max="100" name="attendance" placeholder="Attendance" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label>Behaviour</label>
                            <input type="text" name="behaviour" placeholder="Behaviour" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Result</label>
                            <select name="result" class="form-control" required>
                                <option value="Pass">Pass</option>
                                <option value="Fail">Fail</option>
                            </select>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="dob">Pass Date<span class="text-danger">*</span></label>
                            <input type='text' class="form-control date_picker2" name="pass_date" placeholder="Pass Date" required minlength="10" maxlength="255" />
                            <span class="fa fa-calendar form-control-feedback"></span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-info">Passout</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
<!-- END PAGE CONTENT-->

<!-- BEGIN PAGE JS-->
@section('extraScript')
    <script type="text/javascript">
        $(document).ready(function () {
            var student_id = $(this).data('pk');
            window.postUrl = '{{URL::Route("student.certificateStatus", 0)}}';
            window.section_list_url = '{{URL::Route("academic.section")}}';
            window.changeExportColumnIndex = 7;
            window.excludeFilterComlumns = [0,1,8,9];
            Academic.studentInit();
            $('title').text($('title').text() + '-' + $('select[name="class_id"] option[selected]').text() + '(' + $('select[name="section_id"] option[selected]').text() +')');

            $('.toggle-event').change(function() {
                var student_id = $(this).data('pk');

                $.ajax({
                    url: '{{url("student/certificate-status")}}/'+student_id,
                    type:'GET',
                    success: function(result){
                        if(result.success){
                            toastr.success(result.message);
                        }else{
                            toastr.danger(result.message);
                        }
                    }});
            });
        })
    </script>
    <script type="text/javascript">
        $(document).on("click", ".open-AddBookDialog", function () {
                var student_id = $(".student_pk_id").val();

        });
    </script>



@endsection
<!-- END PAGE JS-->
