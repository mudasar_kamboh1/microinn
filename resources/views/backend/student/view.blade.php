<!-- Master page  -->
@extends('backend.layouts.master')

<!-- Page title -->
@section('pageTitle') student Profile @endsection
<!-- End block -->

<!-- Page body extra css -->
@section('extraStyle')
    <style>
        @media print {
            @page {
                size:  A4 landscape;
                margin: 0;
            }
        }
    </style>
@endsection
<!-- End block -->

<!-- BEGIN PAGE CONTENT-->
@section('pageContent')
    <!-- Section header -->
    <section class="content-header">
        <div class="btn-group">
            <a href="#"  class="btn-ta btn-sm-ta btn-print btn-info btnPrintInformation"><i class="fa fa-print"></i> Print</a>
        </div>
        <div class="btn-group">
            <a href="{{URL::route('student.edit',$student->id)}}" class="btn-ta btn-sm-ta"><i class="fa fa-edit"></i> Edit</a>
        </div>
        <ol class="breadcrumb">
            <li><a href="{{URL::route('user.dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{URL::route('student.index')}}"><i class="fa icon-student"></i> Student</a></li>
            <li class="active">View</li>
        </ol>
    </section>
    <!-- ./Section header -->
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="box box-info">
                            <div class="box-body box-profile">
                                <img  style="height: 100px;" class="profile-user-img img-responsive img-circle" src="@if($student->photo ){{ asset('uploads/students/'.$student->photo)}} @else {{ asset('images/avatar.jpg')}} @endif">
                                <h3 class="profile-username text-center">{{!is_null($student->user) ? $student->user->name : ''}}</h3>
                                <ul class="list-group list-group-unbordered">
                                    <li class="list-group-item" style="background-color: #FFF">
                                        <b>Registration No.</b> <a class="pull-right">{{$student->reg_no}}</a>
                                    </li>
                                    <li class="list-group-item" style="background-color: #FFF">
                                        <b>ID Card No.</b> <a class="pull-right">{{$student->card_no}}</a>
                                    </li>
                                    <li class="list-group-item" style="background-color: #FFF">
                                        <b>Phone</b> <a class="pull-right">{{$student->phone_no}}</a>
                                    </li>
                                    <li class="list-group-item" style="background-color: #FFF">
                                        <b>Email</b> <a class="pull-right">{{$student->email}}</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-9">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#information" data-toggle="tab">Profile</a></li>
                        {{--<li><a href="#routine" data-toggle="tab">Routine</a></li>--}}
                        <li><a href="#attendance" id="tabAttendance" data-pk="{{$student->id}}" data-toggle="tab">Attendance</a></li>
                        {{--<li><a href="#mark" data-toggle="tab">Mark</a></li>--}}
                        {{--<li><a href="#invoice" data-toggle="tab">Invoice</a></li>--}}
                        <li><a href="#payment" data-toggle="tab">Student Courses</a></li>
                        {{--<li><a href="#document" data-toggle="tab">Document</a></li>--}}
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="information">
                            <p class="text-info" style="font-size: 16px;border-bottom: 1px solid #eee;">Personal Info:</p>
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="">Full Name</label>
                                </div>
                                <div class="col-md-3">
                                    <p for="">: {{$student->name}}</p>
                                </div>
                                <div class="col-md-3">
                                    <label for="">Date of Birth</label>
                                </div>
                                <div class="col-md-3">
                                    <p for="">: {{$student->dob}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="">Gender</label>
                                </div>
                                <div class="col-md-3">
                                    <p for="">: {{$student->gender}}</p>
                                </div>
                                <div class="col-md-3">
                                    <label for="">Religion</label>
                                </div>
                                <div class="col-md-3">
                                    <p for="">: {{$student->religion}}</p>
                                </div>
                            </div>
                                <div class="row">
                                <div class="col-md-3">
                                    <label for="">Email</label>
                                </div>
                                <div class="col-md-3">
                                    <p for="">: {{$student->email}}</p>
                                </div>
                                <div class="col-md-3">
                                    <label for="">Phone No.</label>
                                </div>
                                <div class="col-md-3">
                                    <p for="">: {{$student->phone_no}}</p>
                                </div>
                            </div>
                            <p class="text-info" style="font-size: 16px;border-bottom: 1px solid #eee;">Parents Info:</p>
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="">Father Name </label>
                                </div>
                                <div class="col-md-3">
                                    <p for="">: {{$student->father_name}}</p>
                                </div>
                                <div class="col-md-3">
                                    <label for="">Father Phone No.</label>
                                </div>
                                <div class="col-md-3">
                                    <p for="">: {{$student->father_phone_no}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="">Mother Name </label>
                                </div>
                                <div class="col-md-3">
                                    <p for="">: {{$student->mother_name}}</p>
                                </div>
                                <div class="col-md-3">
                                    <label for="">Mother Phone No.</label>
                                </div>
                                <div class="col-md-3">
                                    <p for="">: {{$student->mother_phone_no}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="">Guardian Name </label>
                                </div>
                                <div class="col-md-3">
                                    <p for="">: {{$student->guardian}}</p>
                                </div>
                                <div class="col-md-3">
                                    <label for="">Guardian Phone No.</label>
                                </div>
                                <div class="col-md-3">
                                    <p for="">: {{$student->guardian_phone_no}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="">Present Address </label>
                                </div>
                                <div class="col-md-3">
                                    <p for="">: {{$student->present_address}}</p>
                                </div>
                                <div class="col-md-3">
                                    <label for="">Permanent Address</label>
                                </div>
                                <div class="col-md-3">
                                    <p for="">: {{$student->permanent_address}}</p>
                                </div>
                            </div>
                            <p class="text-info" style="font-size: 16px;border-bottom: 1px solid #eee;">Academic Info:</p>
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="">Academic Year</label>
                                </div>
                                <div class="col-md-3">
                                    <p for="">: {{$student->created_at}}</p>
                                </div>
                                <div class="col-md-3">
                                    <label for="">Registration No </label>
                                </div>
                                <div class="col-md-3">
                                    <p for="">: {{$student->reg_no}}</p>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="">Card No.</label>
                                </div>
                                <div class="col-md-3">
                                    <p for="">: {{$student->card_no}}</p>
                                </div>
                                <div class="col-md-3">
                                    <label for="">Fee Status</label>
                                </div>
                                <div class="col-md-3">
                                    <p for="">:
                                        @if($student->status == 1)
                                            <span class="bg-green badge">Active</span>
                                        @else
                                            <span class="bg-warning badge">Inactive</span>
                                        @endif
                                    </p>
                                </div>
                            </div>


                        </div>
                        {{--<div class="tab-pane" id="routine">--}}
                        {{--</div>--}}
                        <div class="tab-pane" id="attendance">
                            <table id="attendanceTable" class="table table-responsive table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-center">Date</th>
                                        <th class="text-center">Status</th>
                                    </tr>
                                <tbody>

                                </tbody>
                                </thead>
                            </table>
                        </div>
                        <div class="tab-pane" id="mark">

                        </div>
                        <div class="tab-pane" id="invoice">

                        </div>
                        <div class="tab-pane" id="payment">
                            @if(count($student->coursesFee()->get()) > 0)
                                @foreach($student->coursesFee()->get() as $course)
                                <p class="text-info" style="padding:0 0 25px;font-size: 20px;border-bottom: 1px solid #eee;">{{ !is_null($course->course) ? $course->course->name : '-- -- --'}} <button data-toggle="modal" data-target="#fee-plan-modal{{ $loop->iteration }}"  class="btn-ta btn-sm-ta btn-info pull-right"><i class="fa fa-eye"></i> View Fee Plan {{ $loop->iteration }}</button></p>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="">Net Fee</label>
                                    </div>
                                    <div class="col-md-3">
                                        <p for="">: {{$course->net_fee}}</p>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="">Registration Fee</label>
                                    </div>
                                    <div class="col-md-3">
                                        <p for="">: {{$course->reg_fee}}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="">Admission Fee</label>
                                    </div>
                                    <div class="col-md-3">
                                        <p for="">: {{$course->admission_fee}}</p>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="">Test Fee</label>
                                    </div>
                                    <div class="col-md-3">
                                        <p for="">: {{$course->test_fee}}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="">Tuition Fee</label>
                                    </div>
                                    <div class="col-md-3">
                                        <p for="">: {{$course->tuition_fee}}</p>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="">Discount Fee</label>
                                    </div>
                                    <div class="col-md-3">
                                        <p for="">: {{$course->discount_fee}}</p>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="">Discount Reason</label>
                                    </div>
                                    <div class="col-md-3">
                                        <p for="">: {{$course->discount_reason}}</p>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="">Status</label>
                                    </div>
                                    <div class="col-md-3">
                                        @php
                                            $status = $student->feePlans()->where('course_id',$course->course_id)->where('status','Unpaid')->first()
                                        @endphp
                                        <p class="{{ !is_null($status) ? 'label-warning' : 'label-info' }} label">: {{ !is_null($status) ? 'Pending' : 'Paid' }}</p>

                                    </div>
                                </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label for="">Start Date:</label>
                                        </div>
                                        <div class="col-md-3">
                                            <p for="">: {{ \Carbon\Carbon::parse($course->created_at)->format('d-m-Y') }}</p>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="">End Date</label>
                                        </div>
                                        <div class="col-md-3">
                                            <p for="">: {{ \Carbon\Carbon::parse(!is_null($course->course) ? $course->course->end_date : '')->format('d-m-Y') }}</p>
                                        </div>
                                    </div>
                                    <div id="fee-plan-modal{{ $loop->iteration }}" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Fee Plan {{ $loop->iteration }}</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <input type="hidden" id="fee-amount">
                                                    <table class="table table-striped fee-plan-table">
                                                        <thead>
                                                        <th>Sr.</th>
                                                        <th>Date</th>
                                                        <th>Amount</th>
                                                        <th>Balance</th>
                                                        <th>Status</th>
                                                        </thead>
                                                        <tbody>
                                                        @php
                                                            $feePlan = [];
                                                            if(!is_null($student->coursesFee()->first())){
                                                                $feePlan = $student->feePlans()->where('course_id', $course->course_id)->get();
                                                            }
                                                        @endphp

                                                        @if(count($feePlan) > 0)
                                                            @foreach($feePlan as $k => $plan)
                                                                <tr>
                                                                    <td>{{ $loop->iteration }}</td>
                                                                    <td>
                                                                        <div class="has-feedback">
                                                                            <input type="text" value="{{ $plan->pivot->due_date }}" class="form-control" readonly placeholder="Date"/>
                                                                            <span class="fa fa-calendar form-control-feedback"></span>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <input type="number" min="0" class="convert-to-installment form-control" readonly value="{{ $plan->pivot->amount_fee }}">
                                                                    </td>
                                                                    <td>
                                                                        <input type="number" value="{{ $plan->pivot->balance }}" readonly min="0" class="form-control">
                                                                    </td>
                                                                    <td style="padding-top: 15px">
                                                                        <span class="label {{ $plan->pivot->status == 'Paid' ? 'label-info' : 'label-warning' }} ">{{ $plan->pivot->status }}</span>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                        {{--<div class="tab-pane" id="document">--}}
                            {{--<input class="btn btn-success btn-sm" style="margin-bottom: 10px" type="button" value="Add Document" data-toggle="modal" data-target="#documentupload">--}}
                            {{--<div id="hide-table">--}}
                            {{--<table class="table table-striped table-bordered table-hover">--}}
                            {{--<thead>--}}
                            {{--<tr>--}}
                            {{--<th>#</th>--}}
                            {{--<th>Title</th>--}}
                            {{--<th>Date</th>--}}
                            {{--<th>Action</th>--}}
                            {{--</tr>--}}
                            {{--</thead>--}}
                            {{--<tbody>--}}
                            {{--<tr>--}}
                            {{--<td data-title="#">--}}
                            {{--1                                                    </td>--}}

                            {{--<td data-title="Title">--}}
                            {{--Computer                                                    </td>--}}

                            {{--<td data-title="Date">--}}
                            {{--05 Jun 2018                                                    </td>--}}
                            {{--<td data-title="Action">--}}
                            {{--<a href="" class="btn btn-success btn-xs mrg" data-placement="top" data-toggle="tooltip" data-original-title="Download"><i class="fa fa-download"></i></a>--}}
                            {{--<a href="" onclick="return confirm('you are about to delete a record. This cannot be undone. are you sure?')" class="btn btn-danger btn-xs mrg" data-placement="top" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>  --}}
                            {{--</td>--}}
                            {{--</tr>--}}
                            {{--</tbody>--}}
                            {{--</table>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                    </div>
                </div>
                    </div>
                </div>
            </div>

        </div>

    </section>
    <!-- /.content -->
@endsection
<!-- END PAGE CONTENT-->

<!-- BEGIN PAGE JS-->
@section('extraScript')
    <script type="text/javascript">
        window.attendanceUrl = '{{route('student_attendance.index')}}';
        $(document).ready(function () {
           Academic.studentProfileInit();
        });

    </script>
@endsection
