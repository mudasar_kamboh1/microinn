<!-- Master page  -->
@extends('backend.layouts.master')

<!-- Page title -->
@section('pageTitle') Student @endsection
<!-- End block -->

<!-- Page body extra class -->
@section('bodyCssClass') @endsection
<!-- End block -->

<!-- BEGIN PAGE CONTENT-->
@section('pageContent')
    <!-- Section header -->
    <section class="content-header">
        <h1>
            Student
            <small>@if($student) Update @else Add New @endif</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{URL::route('user.dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{URL::route('student.index')}}"><i class="fa icon-student"></i> Student</a></li>
            <li class="active">@if($student) Update @else Add @endif</li>
        </ol>
    </section>
    <!-- ./Section header -->
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <form novalidate id="entryForm" action="@if($student) {{URL::Route('student.update', $student->id)}} @else {{URL::Route('student.store')}} @endif" method="post" enctype="multipart/form-data">
                        <div class="box-body">
                            @csrf
                            @if($student)  {{ method_field('PATCH') }} @endif
                            <p class="lead section-title">Student Info:</p>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="reg_no">Registration No.
                                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="will auto generate after saving the form"></i>
                                        </label>
                                        <input  type="text" class="form-control" name="reg_no" readonly  placeholder="will auto generate after saving the form" value="@if($student){{$student->reg_no}}@endif">
                                        <span class="fa fa-id-card form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('reg_no') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="name">Name<span class="text-danger">*</span></label>
                                        <input autofocus type="text" class="form-control" name="name" placeholder="name" value="@if($student){{ $student->name }}@else{{old('name')}}@endif" required minlength="2" maxlength="255">
                                        <span class="fa fa-info form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="dob">Date of birth<span class="text-danger">*</span></label>
                                        <input type='text' class="form-control date_picker2"  readonly name="dob" placeholder="date" value="@if($student){{ $student->dob }}@else{{old('dob')}}@endif" required minlength="10" maxlength="255" />
                                        <span class="fa fa-calendar form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('dob') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="gender">Gender<span class="text-danger">*</span>
                                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select gender type"></i>
                                        </label>
                                        {!! Form::select('gender', AppHelper::GENDER, $gender , ['class' => 'form-control select2', 'required' => 'true']) !!}
                                        <span class="form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('gender') }}</span>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="religion">Religion<span class="text-danger">*</span>
                                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select religion type"></i>
                                        </label>
                                        {!! Form::select('religion', AppHelper::RELIGION, $religion , ['class' => 'form-control select2', 'required' => 'true']) !!}
                                        <span class="form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('religion') }}</span>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="photo">Photo<span class="text-danger">[min 150 X 150 size and max 200kb]</span></label>
                                        <input  type="file" class="form-control" accept=".jpeg, .jpg, .png" name="photo" placeholder="Photo image">
                                        @if($student && isset($student->photo))
                                            <input type="hidden" name="oldPhoto" value="{{$student->photo}}">
                                        @endif
                                        <span class="glyphicon glyphicon-open-file form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('photo') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="email">Email</label>
                                        <input  type="email" class="form-control" name="email"  placeholder="email address" value="@if($student){{$student->email}}@else{{old('email')}}@endif" maxlength="100" >
                                        <span class="fa fa-envelope form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('email') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="phone_no">Phone/Mobile No.</label>
                                        <input  type="text" class="form-control" name="phone_no" placeholder="phone or mobile number" value="@if($student){{$student->phone_no}}@else{{old('phone_no')}}@endif" maxlength="15">
                                        <span class="fa fa-phone form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('phone_no') }}</span>
                                    </div>
                                </div>
                            </div>
                            <p class="lead  section-title">Guardian Info:</p>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="father_name">Father Name</label>
                                        <input type="text" class="form-control" name="father_name" placeholder="name" value="@if($student){{ $student->father_name }}@else{{old('father_name')}}@endif"  maxlength="255">
                                        <span class="fa fa-info form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('father_name') }}</span>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="father_phone_no">Father Phone/Mobile No.</label>
                                        <input  type="text" class="form-control" name="father_phone_no" placeholder="phone or mobile number" value="@if($student){{$student->father_phone_no}}@else{{old('father_phone_no')}}@endif" maxlength="15">
                                        <span class="fa fa-phone form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('father_phone_no') }}</span>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="mother_name">Mother Name</label>
                                        <input  type="text" class="form-control" name="mother_name" placeholder="name" value="@if($student){{ $student->mother_name }}@else{{old('mother_name')}}@endif" maxlength="255">
                                        <span class="fa fa-info form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('mother_name') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="mother_phone_no">Mother Phone/Mobile No.</label>
                                        <input  type="text" class="form-control" name="mother_phone_no"  placeholder="phone or mobile number" value="@if($student){{$student->mother_phone_no}}@else{{old('mother_phone_no')}}@endif"  maxlength="15">
                                        <span class="fa fa-phone form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('mother_phone_no') }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="blood_group">Blood Group<span class="text-danger">*</span>
                                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select blood group type"></i>
                                        </label>
                                        {!! Form::select('blood_group', AppHelper::BLOOD_GROUP, $bloodGroup , ['class' => 'form-control select2', 'required' => 'true']) !!}
                                        <span class="form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('blood_group') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="nationality">Nationality<span class="text-danger">*</span>
                                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select nationality"></i>
                                        </label>
                                        {!! Form::select('nationality', ['Pakistani' => 'Pakistani', 'Other' => 'Other'], $nationality , ['class' => 'form-control', 'required' => 'true']) !!}
                                        <span class="form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('nationality') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="guardian">Local Guardian</label>
                                        <input  type="text" class="form-control" name="guardian" placeholder="name" value="@if($student){{ $student->guardian }}@else{{old('guardian')}}@endif" maxlength="255">
                                        <span class="fa fa-info form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('guardian') }}</span>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="guardian_phone_no">Guardian Phone/Mobile No.</label>
                                        <input  type="text" class="form-control" name="guardian_phone_no" placeholder="phone or mobile number" value="@if($student){{$student->guardian_phone_no}}@else{{old('guardian_phone_no')}}@endif"  maxlength="15">
                                        <span class="fa fa-phone form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('guardian_phone_no') }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                            <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                        <label for="present_address">Present Address</label>
                                        <textarea name="present_address" class="form-control"  maxlength="500" >@if($student){{ $student->present_address }}@else{{ old('present_address') }} @endif</textarea>
                                        <span class="fa fa-location-arrow form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('present_address') }}</span>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                        <label for="permanent_address">Permanent Address<span class="text-danger">*</span></label>
                                        <textarea name="permanent_address" class="form-control" required minlength="10" maxlength="500" >@if($student){{ $student->permanent_address }}@else{{ old('permanent_address') }} @endif</textarea>
                                        <span class="fa fa-location-arrow form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('permanent_address') }}</span>
                                    </div>
                                </div>
                            </div>
                            <p class="lead section-title">Academic Info:</p>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="campus_id">Campus<span class="text-danger">*</span>
                                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select religion type"></i>
                                        </label>
                                        <select class="form-control select2" name="campus_id" required>
                                            @foreach(\App\Campus::get() as $campus)
                                                <option value="{{ $campus->id }}"> {{ $campus->name }}</option>
                                            @endforeach
                                        </select>
                                        <span class="form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('institute_id') }}</span>
                                    </div>
                                </div>
                                {{--<div class="col-md-3">--}}
                                    {{--<div class="form-group has-feedback">--}}
                                        {{--<label for="institute_id">Study Institute<span class="text-danger">*</span>--}}
                                            {{--<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select religion type"></i>--}}
                                        {{--</label>--}}
                                        {{--<select class="form-control" name="institute_id" id="institute_selector" required>--}}
                                            {{--@foreach(\App\StudyInstitute::get() as $institute)--}}
                                                {{--<option value="{{ $institute->id }}"> {{ $institute->institute }}</option>--}}
                                            {{--@endforeach--}}
                                        {{--</select>--}}
                                        {{--<span class="form-control-feedback"></span>--}}
                                        {{--<span class="text-danger">{{ $errors->first('institute_id') }}</span>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-3">--}}
                                    {{--<div class="form-group has-feedback">--}}
                                        {{--<label for="institute_id">Degree Program<span class="text-danger">*</span>--}}
                                            {{--<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select religion type"></i>--}}
                                        {{--</label>--}}
                                        {{--<select name="degree" class="form-control degreeProgram" id="degreePrograms">--}}
                                            {{--<option value="">Select</option>--}}
                                            {{--@foreach(\App\DegreeProgram::get() as $institute)--}}
                                                {{--<option value="{{$institute->id}}">{{$institute->title}}</option>--}}
                                            {{--@endforeach--}}
                                        {{--</select>--}}

                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-3">--}}
                                    {{--<div class="form-group has-feedback">--}}
                                        {{--<label for="institute_id">Study Program<span class="text-danger">*</span>--}}
                                            {{--<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select religion type"></i>--}}
                                        {{--</label>--}}
                                        {{--<select name="study" class="form-control select2 studyProgram" id="studyProgram">--}}
                                            {{--<option value="">Select</option>--}}
                                            {{--@foreach(\App\StudyProgram::get() as $institute)--}}
                                                {{--<option value="{{$institute->id}}">{{$institute->title}}</option>--}}
                                            {{--@endforeach--}}
                                        {{--</select>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="row">--}}
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="shift">Shift
                                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Set class shift"></i>
                                        </label>
                                        {!! Form::select('shift', ['Morning' => 'Morning', 'Day' => 'Day', 'Evening' => 'Evening' ], $shift , ['placeholder' => 'Pick a shift...','class' => 'form-control select2', 'required' => 'true']) !!}
                                        <span class="form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('shift') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="card_no">ID Card No.</label>
                                        <input  type="text" class="form-control" name="card_no"  placeholder="id card number" value="@if($student){{$student->card_no}}@else{{old('card_no')}}@endif"  min="4" maxlength="50">
                                        <span class="fa fa-id-card form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('card_no') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="roll_no">Roll No.</label>
                                        <input  type="number" class="form-control" name="roll_no"  placeholder="roll number" value="@if($student){{$student->roll_no}}@else{{old('roll_no')}}@endif"  maxlength="20">
                                        <span class="fa fa-sort-numeric-asc form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('roll_no') }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="roll_no">Study Days.</label>
                                    <div class="form-group has-feedback">
                                        <span class="pull-left" style="width:14%;"><b style="width:100%;float: left">Mon</b><input type="checkbox" name="study_days[]" {{ !is_null($student) && ($student->study_days) && collect(explode(',',$student->study_days))->contains(1) ? 'checked' : '' }} class="form-control pull-left" value="1"></span>
                                        <span class="pull-left" style="width:14%;"><b style="width:100%;float: left">Tue</b> <input type="checkbox" name="study_days[]" {{ !is_null($student) && ($student->study_days) && collect(explode(',',$student->study_days))->contains(2) ? 'checked' : '' }} class="form-control pull-left" value="2"></span>
                                        <span class="pull-left" style="width:14%;"><b style="width:100%;float: left">Wed</b> <input type="checkbox" name="study_days[]" {{ !is_null($student) && ($student->study_days) && collect(explode(',',$student->study_days))->contains(3) ? 'checked' : '' }} class="form-control pull-left" value="3"></span>
                                        <span class="pull-left" style="width:14%;"><b style="width:100%;float: left">Thu</b> <input type="checkbox" name="study_days[]" {{ !is_null($student) && ($student->study_days) && collect(explode(',',$student->study_days))->contains(4) ? 'checked' : '' }} class="form-control pull-left" value="4"></span>
                                        <span class="pull-left" style="width:14%;"><b style="width:100%;float: left">Fri</b> <input type="checkbox" name="study_days[]" {{ !is_null($student) && ($student->study_days) && collect(explode(',',$student->study_days))->contains(5) ? 'checked' : '' }} class="form-control pull-left" value="5"></span>
                                        <span class="pull-left" style="width:14%;"><b style="width:100%;float: left">Sat</b> <input type="checkbox" name="study_days[]" {{ !is_null($student) && ($student->study_days) && collect(explode(',',$student->study_days))->contains(6) ? 'checked' : '' }} class="form-control pull-left" value="6"></span>
                                        <span class="pull-left" style="width:14%;"><b style="width:100%;float: left">Sun</b> <input type="checkbox" name="study_days[]" {{ !is_null($student) && ($student->study_days) && collect(explode(',',$student->study_days))->contains(7) ? 'checked' : '' }} class="form-control pull-left" value="7"></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                        <label for="note">Note</label>
                                        <textarea name="note" class="form-control"  maxlength="500">@if($student){{ $student->note }}@else{{ old('note') }} @endif</textarea>
                                        <span class="fa fa-info form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('note') }}</span>
                                    </div>
                                </div>
                            </div>
                            <p class="lead section-title">Fee Structure: <button class="btn btn-success pull-right" type="button" id="course-add"><i class="fa fa-plus-square"></i> Add More Courses</button></p>
                            <div class="row">
                                <div class="col-sm-12">
                                    <table class="table table-info" id="course-table">
                                        <thead>
                                        <tr>
                                            <th>Course</th>
                                            <th>Reg Fee</th>
                                            <th>Admission Fee</th>
                                            <th>Test Fee</th>
                                            <th>Tuition Fee</th>
                                            <th>Discount</th>
                                            <th>Net Fee</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                         @if(!is_null($student) && !is_null($student->coursesFee()->first()))
                                             <tr>
                                                 <td width="250px">
                                                     <select name="course_id" class="form-control select2 course">
                                                         <option value="">Select Course</option>
                                                         @foreach(\App\Course::get() as $course)
                                                             <option value="{{$course->id}}" {{ !is_null($student) && !is_null($student->coursesFee()->first()) && $course->id == $student->coursesFee()->first()->course_id ? 'selected' : '' }}>{{$course->name}} | {{ !is_null($course->campus) ? $course->campus->name : '' }}</option>
                                                         @endforeach
                                                     </select>
                                                 </td>
                                                 <td>
                                                     <input type="number" name="reg_fee" value="{{ !is_null($student) && !is_null($student->coursesFee()->first()) ?  $student->coursesFee()->first()->reg_fee : '' }}" class="form-control">
                                                 </td>
                                                 <td>
                                                     <input type="number" name="admission_fee" value="{{ !is_null($student) && !is_null($student->coursesFee()->first()) ?  $student->coursesFee()->first()->admission_fee : '' }}" class="form-control">
                                                 </td>
                                                 <td><input type="number" name="test_fee" value="{{ !is_null($student) && !is_null($student->coursesFee()->first()) ?  $student->coursesFee()->first()->test_fee : '' }}" class="form-control"></td>
                                                 <td><input type="number" name="tuition_fee" value="{{ !is_null($student) && !is_null($student->coursesFee()->first()) ?  $student->coursesFee()->first()->tuition_fee : '' }}" class="form-control"></td>
                                                 <td><input style="width:75%;float: left" type="number" name="discount_fee" value="{{ !is_null($student) && !is_null($student->coursesFee()->first()) ?  $student->coursesFee()->first()->discount_fee : '' }}" class="form-control discount">
                                                     <button type="button" data-toggle="modal" data-target="#discount-modal"  class="btn btn-sm btn-info pull-right"><i class="fa fa-info"></i></button>
                                                     <div id="discount-modal" class="modal fade" role="dialog">
                                                         <div class="modal-dialog">

                                                             <!-- Modal content-->
                                                             <div class="modal-content">
                                                                 <div class="modal-header">
                                                                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                     <h4 class="modal-title">Discount Reason</h4>
                                                                 </div>
                                                                 <div class="modal-body">
                                                                     <input type="text" name="discount_reason" placeholder="Reason" value="{{ !is_null($student) && !is_null($student->coursesFee()->first()) ?  $student->coursesFee()->first()->discount_reason : '' }}" class="form-control">
                                                                 </div>
                                                                 <div class="modal-footer">
                                                                     <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                                 </div>
                                                             </div>

                                                         </div>
                                                     </div>
                                                 </td>
                                                 <td><input type="number" name="net_fee" value="{{ !is_null($student) && !is_null($student->coursesFee()->first()) ?  $student->coursesFee()->first()->net_fee : '' }}"  class="form-control"></td>
                                                 <td width="190px">
                                                     @if(!is_null($student) && !is_null($student->feePlans))
                                                        <button type="button" data-toggle="modal" data-target="#fee-plan-modal1" class="btn btn-md btn-info pull-left">View Fee Plan</button><button type="button" class="btn btn-md btn-danger pull-right delete-row"><i class="fa fa-trash"></i></button>
                                                        <div id="fee-plan-modal1" class="modal fade" role="dialog">
                                                             <div class="modal-dialog">

                                                                 <!-- Modal content-->
                                                                 <div class="modal-content">
                                                                     <div class="modal-header">
                                                                         <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                         <h4 class="modal-title">Fee Plan</h4>
                                                                     </div>
                                                                     <div class="modal-body">
                                                                         <input type="hidden" id="fee-amount">
                                                                         <table class="table table-striped fee-plan-table">
                                                                             <thead>
                                                                             <th>Sr.</th>
                                                                             <th>Date</th>
                                                                             <th>Amount</th>
                                                                             <th>Balance</th>
                                                                             <th>Status</th>
                                                                             </thead>
                                                                             <tbody>
                                                                                @php
                                                                                    $feePlan = [];
                                                                                    if(!is_null($student->coursesFee()->first())){
                                                                                        $feePlan = $student->feePlans()->where('course_id', $student->coursesFee()->first()->course_id)->get();
                                                                                    }
                                                                                @endphp

                                                                                @if(count($feePlan) > 0)
                                                                                    @foreach($feePlan as $k => $plan)
                                                                                         <tr>
                                                                                             <td>{{ $loop->iteration }}</td>
                                                                                             <td>
                                                                                                 <div class="has-feedback">
                                                                                                     <input type="text" value="{{ $plan->pivot->due_date }}" class="form-control" readonly placeholder="Date"/>
                                                                                                     <span class="fa fa-calendar form-control-feedback"></span>
                                                                                                 </div>
                                                                                             </td>
                                                                                             <td>
                                                                                                 <input type="number" min="0" class="convert-to-installment form-control" readonly value="{{ $plan->pivot->amount_fee }}">
                                                                                             </td>
                                                                                             <td>
                                                                                                 <input type="number" value="{{ $plan->pivot->balance }}" readonly min="0" class="form-control">
                                                                                             </td>
                                                                                             <td style="padding-top: 15px">
                                                                                                 <span class="label {{ $plan->pivot->status == 'Paid' ? 'label-info' : 'label-warning' }} ">{{ $plan->pivot->status }}</span>
                                                                                             </td>
                                                                                         </tr>
                                                                                    @endforeach
                                                                                @endif
                                                                             </tbody>
                                                                         </table>
                                                                     </div>
                                                                     <div class="modal-footer">
                                                                         <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                                     </div>
                                                                 </div>

                                                             </div>
                                                         </div>
                                                     @else
                                                        <button type="button" class="btn btn-md btn-info pull-left generate-fee-plan">Generate Fee Plan</button><button type="button" class="btn btn-md btn-danger pull-right delete-row"><i class="fa fa-trash"></i></button>
                                                     @endif
                                                 </td>
                                             </tr>
                                         @endif
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                            @if(!$student)<p class="lead section-title">Access Info:</p>@endif
                                <div class="row">
                                    @if(!$student)
                                    <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="username">Username</label>
                                        <input  type="text" class="form-control" value="" name="username" placeholder="leave blank if not need to create user"  minlength="5" maxlength="255">
                                        <span class="glyphicon glyphicon-info-sign form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('username') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="password">Passwrod</label>
                                        <input type="password" class="form-control" name="password" placeholder="leave blank if not need to create user"  minlength="6" maxlength="50">
                                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('password') }}</span>
                                    </div>
                                </div>
                                @endif
                            </div>


                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <a href="{{URL::route('student.index')}}" class="btn btn-default">Cancel</a>
                            <button type="submit" class="btn btn-info pull-right"><i class="fa @if($student) fa-refresh @else fa-plus-circle @endif"></i> @if($student) Update @else Add @endif</button>

                        </div>

                        <!-- Modal -->
                        <div id="fee-plan-modal" class="modal fade" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Fee Plan</h4>
                                    </div>
                                    <div class="modal-body">
                                        <input type="hidden" id="fee-amount">
                                        <table class="table table-striped fee-plan-table">
                                            <thead>
                                            <th>Sr.</th>
                                            <th>Date</th>
                                            <th>Amount</th>
                                            <th>Balance</th>
                                            <th>Action</th>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
<!-- END PAGE CONTENT-->

<!-- BEGIN PAGE JS-->
@section('extraScript')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            window.section_list_url = '{{URL::Route("academic.section")}}';
            window.subject_list_url = '{{URL::Route("academic.subject")}}';
            Academic.studentInit();

        });

        $('#institute_selector').change(function () {
            var selectedInstitute = $(this).children("option:selected").val();
            $.ajax({
                url: '/degree/' + selectedInstitute + '/degreeProgram',
                method: 'GET',
                type: 'JSON',
                success: function (response) {
                    $(".degreeProgram").replaceWith(response.view);
                }
            })
        });

        $(document).on('change','.course',function () {
            var course_id = $(this).val();
            var selector = $(this).parents('tr');
            $.ajax({
                url: "{{ url('admin/course') }}/"+course_id,
                method: 'GET',
                type: 'JSON',
                dataType : 'json',
                contentType: "application/json",
                success: function (response) {
                    selector.find('input[name="reg_fee"]').val(response.registration_fee);
                    selector.find('input[name="admission_fee"]').val(response.admission_fee);
                    selector.find('input[name="test_fee"]').val(response.test_fee);
                    selector.find('input[name="tuition_fee"]').val(response.tution_fee);
                    selector.find('input[name="discount_fee"]').val(0);
                    var total = parseInt(response.registration_fee) + parseInt(response.admission_fee) + parseInt(response.test_fee) + parseInt(response.tution_fee)
                    selector.find('input[name="net_fee"]').val(total);
                }
            });
        });

        $(document).on('change','input[name="discount_fee"]',function () {
            var selector = $(this).parents('tr');
            var reg_fee = selector.find('input[name="reg_fee"]').val();
            var admission_fee = selector.find('input[name="admission_fee"]').val();
            var test_fee= selector.find('input[name="test_fee"]').val();
            var tuition_fee = selector.find('input[name="tuition_fee"]').val();
            var discount_fee = $(this).val();
            var total = parseInt(reg_fee) + parseInt(admission_fee) + parseInt(test_fee) + parseInt(tuition_fee)
            total = total - parseInt(discount_fee);
            selector.find('input[name="net_fee"]').val(total);
            $('#discount-modal').modal('show');
        });

        $(document).on('click','.generate-fee-plan',function () {
            var selector = $(this).parents('tr');
            if($('#fee-plan-modal tbody').children('tr').length < 1) {
                var reg_fee = selector.find('input[name="reg_fee"]').val();
                var admission_fee = selector.find('input[name="admission_fee"]').val();
                var test_fee = selector.find('input[name="test_fee"]').val();
                var tuition_fee = selector.find('input[name="tuition_fee"]').val();
                var discount_fee = selector.find('input[name="discount_fee"]').val();
                var total = parseInt(reg_fee) + parseInt(admission_fee) + parseInt(test_fee) + parseInt(tuition_fee)
                total = total - parseInt(discount_fee);
                var date = new Date();
                date = date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();
                var html = '<tr><td>1</td><td><div class="form-group has-feedback"> <input type="text" value="' + date + '" class="form-control date_picker2" name="fee_plan_date[]" placeholder="Date"/><span class="fa fa-calendar form-control-feedback"></span></div></td><td><input type="number" name="amount[]" min="0" class="convert-to-installment form-control" value="' + total + '"></td><td><input type="number" value="' + total + '" readonly min="0" name="balance[]" class="form-control"></td><td><button type="button" class="btn btn-sm btn-danger delete-row"><i class="fa fa-trash"></i></button></td></tr>'
                $('#fee-plan-modal tbody').append(html);
                $(".date_picker2").datetimepicker({
                    format: "DD/MM/YYYY",
                    viewMode: "years",
                    ignoreReadonly: !0
                });
            }
            $('#fee-plan-modal').modal('show');
        });

        $(document).on('click','.delete-row',function () {
            var selector = $(this).parents('tr');
            var count = selector.parent().children().length;
            if(count > 1){
                selector.remove();
            }else{
                toastr.warning('You can delete it even its one record only!')
            }
        });

        $(document).on('change','.convert-to-installment',function () {
            var selector = $(this).parents('tr');
            selector.siblings().remove();
            if(selector.parent().children().length == 1){
                var payAmount = $(this).val();
                var balance = selector.find('input[name="balance[]"]').val();

                var i = 1;
                if(payAmount == 0){
                    toastr.warning('Please enter valid amount!')
                    return false;
                }
                while(balance >= 0){
                    var date = moment();
                    var nextDate = moment(date).add(i, 'M');
                    nextDate = nextDate.format('DD/MM/YYYY');

                    var count = selector.parent().children().length;
                    balance = balance - payAmount;
                    if(balance > 0){
                        var html = '';
                        if(balance > payAmount){
                            html = '<tr><td>'+(count + 1)+'</td><td><div class="form-group has-feedback"> <input type="text" value="'+nextDate+'" class="form-control date_picker2" name="fee_plan_date[]" placeholder="Date"/><span class="fa fa-calendar form-control-feedback"></span></div></td><td><input type="number" name="amount[]" readonly class="convert-to-installment form-control" min="0" value="'+payAmount+'"></td><td><input type="number" readonly value="'+balance+'" min="0" name="balance[]" class="form-control"></td><td><button type="button" class="btn btn-sm btn-danger delete-row"><i class="fa fa-trash"></i></button></td></tr>'
                        }else{
                            html = '<tr><td>'+(count + 1)+'</td><td><div class="form-group has-feedback"> <input type="text" value="'+nextDate+'" class="form-control date_picker2" name="fee_plan_date[]" placeholder="Date"/><span class="fa fa-calendar form-control-feedback"></span></div></td><td><input type="number" name="amount[]" readonly class="convert-to-installment form-control" min="0" value="'+balance+'"></td><td><input type="number" readonly value="'+balance+'" min="0" name="balance[]" class="form-control"></td><td><button type="button" class="btn btn-sm btn-danger delete-row"><i class="fa fa-trash"></i></button></td></tr>'
                        }
                        $('#fee-plan-modal tbody').append(html);
                        $(".date_picker2").datetimepicker({
                            format: "DD/MM/YYYY",
                            viewMode: "years",
                            ignoreReadonly: !0
                        });
                    }
                    i++;
                }
            }
        });

        $('#course-add').click(function () {
            if($('#course-table tbody').children('tr').length < 1){
                var html = '<tr><td width="250px"><select name="course_id" class="form-control select2 course""><option value="">Select Course</option>@foreach(\App\Course::get() as $course)<option value="{{$course->id}}">{{$course->name}} | {{ !is_null($course->campus) ? $course->campus->name : '' }}</option>@endforeach</select></td><td><input type="number" name="reg_fee" class="form-control"></td><td><input type="number" name="admission_fee" class="form-control"></td><td><input type="number" name="test_fee" class="form-control"></td><td><input type="number" name="tuition_fee" class="form-control"></td><td><input type="number" name="discount_fee" class="form-control"></td><td><input type="number" name="net_fee" class="form-control"></td><td width="190px"><button type="button" class="btn btn-md btn-info pull-left generate-fee-plan">Generate Fee Plan</button><button type="button" class="btn btn-md btn-danger pull-right delete-row"><i class="fa fa-trash"></i></button></td></tr>'
                $('#course-table tbody').append(html);
            }else{
                toastr.info('You can add just one course for now. Multiple option will be availabel soon!');
            }
        });

        $("input").keydown(function(e){
            if (e.keyCode == 13) {
                e.preventDefault();
            };
        });
    </script>
@endsection
<!-- END PAGE JS-->
