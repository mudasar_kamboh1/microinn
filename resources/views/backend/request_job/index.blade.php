<!-- Master page  -->
@extends('backend.layouts.master')

{{--<!-- Page title -->--}}
@section('pageTitle') Events @endsection
{{--<!-- End block -->--}}

{{--<!-- Page body extra class -->--}}
@section('bodyCssClass') @endsection
{{--<!-- End block -->--}}

{{--<!-- BEGIN PAGE CONTENT-->--}}
@section('pageContent')
    {{--    <!-- Section header -->--}}
    <section class="content-header">
        <h1>
            Jobs
            <small>List</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{URL::route('site.dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#"><i class="fa icon-teacher"></i> Jobs</a></li>

        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title">Jobs</h3>

                        <div class="box-tools pull-right">
                            <a class="btn btn-info btn-sm" href="{{ URL::route('jobs.create') }}"><i class="fa fa-plus-circle"></i> Add New</a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="eventList" class="table table-bordered table-striped list_view_table">
                            <thead>
                            <tr>
                                <th>Title</th>
                                <th>Name</th>
                                <th>Location</th>
                                <th>Name</th>
                                {{--                                <th width="30%">cnic</th>--}}
                                <th width="150px">Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($requestJobs as $job)
                                <tr>

                                    <td>
                                        {{ $job->title }}
                                    </td>
                                    <td> {{ $job->name }}</td>
                                    <td> {{ $job->location }}</td>
                                    <td> {{ $job->name }}</td>
                                    {{--                                    <td> {{ $job->cnic }}</td>--}}
                                    {{--                                    <td> {{ $job->name }}</td>--}}
                                    <td>
                                        <div class="btn-group" style="float: left; margin-left: 10px;">
                                            <a title="Edit" href="{{URL::route('job-requests.edit',$job->id)}}" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></a>

                                        </div>
                                        <div class="btn-group" style="float: left; margin-left: 10px;">
                                            <a title="Detail" href="{{URL::route('job-requests.show',$job->id)}}" class="btn btn-info btn-sm"><i class="fa fa-address-book-o"></i></a>

                                        </div>
                                        <div class="btn-group" style="float: left; margin-left: 10px;">
                                            <form class="myAction" method="POST" action="{{URL::route('job-requests.destroy',$job->id)}}">
                                                {{ method_field('DELETE') }}
                                                @csrf
                                                <button type="submit" class="btn btn-danger btn-sm" title="Delete">
                                                    <i class="fa fa-fw fa-trash"></i>
                                                </button>
                                            </form>
                                        </div>

                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        {{ $requestJobs->links() }}
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>

    </section>
    <!-- /.content -->
@endsection
<!-- END PAGE CONTENT-->

<!-- BEGIN PAGE JS-->
@section('extraScript')
    <script type="text/javascript">
        $(document).ready(function () {
            Site.EventInit();
            initDeleteDialog();
        });
    </script>
@endsection
<!-- END PAGE JS-->
