<!-- Master page  -->
@extends('backend.layouts.master')

<!-- Page title -->
@section('pageTitle') Events @endsection
<!-- End block -->

<!-- BEGIN PAGE CSS-->
@section('extraStyle')
@endsection

<!-- Page body extra class -->
@section('bodyCssClass') @endsection
<!-- End block -->

<!-- BEGIN PAGE CONTENT-->
@section('pageContent')
    <!-- Section header -->
    <section class="content-header">
        <h1>
            Job Request Detail
            <small>List</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{URL::route('site.dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{URL::route('job-requests.index')}}"><i class="fa icon-Event"></i> Job Request</a></li>
            <li class="active">Job Request Detail</li>
        </ol>
    </section>
    <!-- ./Section header -->
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="form-group has-feedback">
                                <label for="title">Title:</label><br/>
                                <span class="text-info">{{ $request->title }}</span>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="title">Location</label>
                                <span class="text-info">{{ $request->location }}</span>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="title">Name</label>
                                <span class="text-info">{{ $request->name }}</span>

                            </div>
                            <div class="form-group has-feedback">
                                <label for="title">Cnic</label>
                                <span class="text-info">{{ $request->cnic }}</span>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="event_time">Date of Birth</label>
                                <span class="text-info">{{ $request->dob,date('d/m/Y h:i a')}}</span>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="title">Gender</label>
                                <span class="text-info">{{ $request->gender==0?'Female':"Male"}}</span>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="title">Marital Status</label>
                                <span class="text-info">{{ $request->gender==0?'Single':"Double"}}</span>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="title">cell no</label>
                                <span class="text-info">{{ $request->cell_no }}</span>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="title">Email</label>
                                <span class="text-info">{{ $request->email }}</span>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="title">Residence</label>
                                <span class="text-info">{{ $request->residence }}</span>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="title">Educational</label>
                                <span class="text-info">{{ $request->educational }}</span>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="title">Professional</label>
                                <span class="text-info">{{ $request->Professional }}</span>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="title">Experience</label>
                                <span class="text-info">{{ $request->experience }}</span>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="title">Reference</label>
                                <span class="text-info">{{ $request->reference }}</span>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="cover_photo">Photo<span class="text-danger"></span></label>
                                <span class="text-info">
{{--                                    {{ asset('storage/job/requests')}}/{{ $job->photograph}}--}}
{{--                                    {{ asset('storage/job/requests/').$request->photograph }}--}}
                                    @if (pathinfo(asset('storage/job/requests/'.$request->photograph), PATHINFO_EXTENSION) == 'png' || pathinfo(asset('storage/job/requests/'.$request->photograph), PATHINFO_EXTENSION) == 'jpg')
                                        <img class="w-100" height="70px" src="{{ asset('storage/job/requests/'.$request->photograph) }}" >
                                    @else
                                        <img class="w-100" src="{{ asset('images/default/event.jpg')}}" >
                                    @endif


                                </span>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
<!-- END PAGE CONTENT-->
<!-- BEGIN PAGE JS-->
@section('extraScript')
    <!-- editor js -->
    <script>
        //editor jQuery Patch fixing
        jQuery = $;
    </script>
    <script src="{{ asset('/js/editor.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            Site.EventInit();
        });
    </script>
@endsection
<!-- END PAGE JS-->
