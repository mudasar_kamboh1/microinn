<!-- Master page  -->
@extends('backend.layouts.master')

<!-- Page title -->
@section('pageTitle') Events @endsection
<!-- End block -->

<!-- BEGIN PAGE CSS-->
@section('extraStyle')
@endsection

<!-- Page body extra class -->
@section('bodyCssClass') @endsection
<!-- End block -->

<!-- BEGIN PAGE CONTENT-->
@section('pageContent')
    <!-- Section header -->
    <section class="content-header">
        <h1>
            Events
            <small>List</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{URL::route('site.dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{URL::route('event.index')}}"><i class="fa icon-Event"></i> Events</a></li>
            <li class="active">@if($request) Update @else Add @endif</li>
        </ol>
    </section>
    <!-- ./Section header -->
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <form novalidate id="EventForm" action=" {{URL::Route('job-requests.update', $request->id)}} " method="post" enctype="multipart/form-data">
                        <div class="box-header">
                            <h3 class="box-title">Update Job Request<span class="text-danger"> * Marks are required feild</span></h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            @csrf
                            @if($request) {{ method_field('PATCH') }} @endif
                            <div class="form-group has-feedback">
                                <label for="title">Title<span class="text-danger">*</span></label>
                                <input autofocus type="text" class="form-control" name="title" placeholder="name" value="@if($request){{ $request->title }}@else{{ old('title') }} @endif" >
                                <span class="glyphicon glyphicon-info form-control-feedback"></span>
                                <span class="text-danger">{{ $errors->first('title') }}</span>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="title">Location<span class="text-danger">*</span></label>
                                <input autofocus type="text" class="form-control" name="location" placeholder="Job Locator" value="@if($request){{ $request->location }}@else{{ old('location') }} @endif">
                                <span class="glyphicon glyphicon-info form-control-feedback"></span>
                                <span class="text-danger">{{ $errors->first('location') }}</span>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="title">Name<span class="text-danger">*</span></label>
                                <input autofocus type="text" class="form-control" name="name" placeholder="name" value="@if($request){{ $request->name }}@else{{ old('name') }} @endif" required>
                                <span class="glyphicon glyphicon-info form-control-feedback"></span>
                                <span class="text-danger">{{ $errors->first('name') }}</span>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="title">Cnic<span class="text-danger">*</span></label>
                                <input autofocus type="text" class="form-control" name="cnic" placeholder="cnic" value="@if($request){{ $request->cnic }}@else{{ old('cnic') }} @endif">
                                <span class="glyphicon glyphicon-info form-control-feedback"></span>
                                <span class="text-danger">{{ $errors->first('cnic') }}</span>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="event_time">Date of Birth<span class="text-danger">*</span></label>
                                <input type='text' class="form-control event_time"  readonly name="dob" placeholder="sate of birth" value="@if($request){{ $request->dob }}@else{{ old('dob',date('d/m/Y h:i a')) }} @endif"  />
                                <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
                                <span class="text-danger">{{ $errors->first('dob') }}</span>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="title">Gender<span class="text-danger">*</span></label><br/>
                                <select name="gender" class="form-control" >
                                    <option value="">Select</option>
                                    <option value="0">Female</option>
                                    <option value="1">Male</option>
                                </select>
                               </div>
                            <div class="form-group has-feedback">
                                <label for="title">Marital Status<span class="text-danger">*</span></label><br/>
                                <select name="martial_status" class="form-control">
                                    <option value="">Select</option>
                                    <option value="0">Single</option>
                                    <option value="1">Double</option>
                                </select>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="title">cell no<span class="text-danger">*</span></label>
                                <input autofocus type="text" class="form-control" name="cell_no" placeholder="cell no" value="@if($request){{ $request->cell_no }}@else{{ old('cell_no') }} @endif">
                                <span class="glyphicon glyphicon-info form-control-feedback"></span>
                                <span class="text-danger">{{ $errors->first('cell_no') }}</span>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="title">Email<span class="text-danger">*</span></label>
                                <input autofocus type="email" class="form-control" name="email" placeholder="email" value="@if($request){{ $request->email }}@else{{ old('email') }} @endif">
                                <span class="glyphicon glyphicon-info form-control-feedback"></span>
                                <span class="text-danger">{{ $errors->first('email') }}</span>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="title">Residence<span class="text-danger">*</span></label>
                                <input autofocus type="text" class="form-control" name="residence" placeholder="residence" value="@if($request){{ $request->residence }}@else{{ old('residence') }} @endif">
                                <span class="glyphicon glyphicon-info form-control-feedback"></span>
                                <span class="text-danger">{{ $errors->first('residence') }}</span>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="title">Educational<span class="text-danger">*</span></label>
                                <input autofocus type="text" class="form-control" name="educational" placeholder="educational" value="@if($request){{ $request->educational }}@else{{ old('educational') }} @endif">
                                <span class="glyphicon glyphicon-info form-control-feedback"></span>
                                <span class="text-danger">{{ $errors->first('educational') }}</span>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="title">Professional<span class="text-danger">*</span></label>
                                <input autofocus type="text" class="form-control" name="Professional" placeholder="Professional" value="@if($request){{ $request->Professional }}@else{{ old('Professional') }} @endif">
                                <span class="glyphicon glyphicon-info form-control-feedback"></span>
                                <span class="text-danger">{{ $errors->first('Professional') }}</span>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="title">Experience<span class="text-danger">*</span></label>
                                <input autofocus type="text" class="form-control" name="experience" placeholder="experience" value="@if($request){{ $request->experience }}@else{{ old('experience') }} @endif">
                                <span class="glyphicon glyphicon-info form-control-feedback"></span>
                                <span class="text-danger">{{ $errors->first('experience') }}</span>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="title">Reference<span class="text-danger">*</span></label>
                                <input autofocus type="text" class="form-control" name="reference" placeholder="reference" value="@if($request){{ $request->reference }}@else{{ old('reference') }} @endif">
                                <input type="hidden" name="institute_job_id"  value="{{ $request->institute_job_id }}">
                                <span class="glyphicon glyphicon-info form-control-feedback"></span>
                                <span class="text-danger">{{ $errors->first('reference') }}</span>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="cover_photo">Photo<span class="text-danger"></span></label>
                                <input  type="file" class="form-control" accept=".jpeg, .jpg, .png" name="photograph"  >
                                <span class="glyphicon glyphicon-open-file form-control-feedback"></span>
                                <span class="text-danger">{{ $errors->first('photograph') }}</span>
                            </div>

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <a href="{{URL::route('event.index')}}" class="btn btn-default">Cancel</a>
                            <button type="submit" class="btn btn-info pull-right"><i class="fa @if($request) fa-refresh @else fa-plus-circle @endif"></i> @if($request) Update @else Add @endif</button>

                        </div>
                        {{--@if (count($errors) > 0)--}}
                        {{--<div class="error">--}}
                        {{--<ul>--}}
                        {{--@foreach ($errors->all() as $error)--}}
                        {{--<li>{{ $error }}</li>--}}
                        {{--@endforeach--}}
                        {{--</ul>--}}
                        {{--</div>--}}
                        {{--@endif--}}
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
<!-- END PAGE CONTENT-->


<!-- BEGIN PAGE JS-->
@section('extraScript')
    <!-- editor js -->
    <script>
        //editor jQuery Patch fixing
        jQuery = $;
    </script>
    <script src="{{ asset('/js/editor.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            Site.EventInit();



        });
    </script>
@endsection
<!-- END PAGE JS-->
