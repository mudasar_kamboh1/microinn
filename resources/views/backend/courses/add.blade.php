<!-- Master page  -->
@extends('backend.layouts.master')

<!-- Page title -->
@section('pageTitle') Campus @endsection
<!-- End block -->

<!-- Page body extra class -->
@section('bodyCssClass') @endsection
<!-- End block -->

<!-- BEGIN PAGE CONTENT-->
@section('pageContent')
    <!-- Section header -->
    <section class="content-header">
        <h1>
            Course
            <small>@if($course) Update @else Add New @endif</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{URL::route('user.dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{URL::route('course.index')}}"><i class="fa icon-Campus"></i> Course</a></li>
            <li class="active">@if($course) Update @else Add @endif</li>
        </ol>
    </section>
    <!-- ./Section header -->
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <form novalidate id="entryForm" action="@if($course) {{URL::Route('course.update', $course->id)}} @else {{URL::Route('course.store')}} @endif" method="post" enctype="multipart/form-data">
                        <div class="box-body">
                            @csrf
                            @if($course)  {{ method_field('PATCH') }} @endif
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group has-feedback">
                                        <label for="name">Code<span class="text-danger">*</span></label>
                                        <input autofocus type="text" class="form-control" placeholder="Code" value="@if($course){{ $course->code }}@else{{old('code')}}@endif" required maxlength="30" disabled="disabled">
                                        <span class="fa fa-code form-control-feedback"></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="name">Name<span class="text-danger">*</span></label>
                                        <input autofocus type="text" class="form-control" name="name" placeholder="name" value="@if($course){{ $course->name }}@else{{old('name')}}@endif" required >
                                        <span class="fa fa-info form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                        <label for="name">Detail<span class="text-danger">*</span></label>
                                        <input autofocus type="text" class="form-control" name="detail" placeholder="address" value="@if($course){{ $course->detail }}@else{{old('detail')}}@endif" required >
                                        <span class="fa fa-map-marker form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('detail') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="name">Institute<span class="text-danger">*</span></label>
                                        <select class="form-control" name="institute_id">
                                            @foreach(\App\StudyInstitute::get() as $institute)
                                                <option value="{{$institute->id }}" {{ $course && $course->institute_id == $institute->id ? 'selected' : ''}}>{{ $institute->institute }}</option>
                                            @endforeach
                                        </select>
                                        <span class="text-danger">{{ $errors->first('institute_id') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="name">Duration In Days No.<span class="text-danger">*</span></label>
                                        <input autofocus type="number" class="form-control" name="duration" placeholder="duration" value="@if($course){{ $course->duration }}@else{{old('duration')}}@endif" required >
                                        <span class="fa fa-sun-o form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('duration') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="name">End Date Apply<span class="text-danger">*</span></label>
                                        <select class="form-control" name="is_end" onchange="disableDate($(this).val())">
                                            <option value="0" {{ $course && $course->is_end == 0 ? 'selected' : ''}}>No</option>
                                            <option value="1" {{ $course && $course->is_end == 1 ? 'selected' : ''}}>Yes</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="name">Session End Date<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control date_picker2" name="end_date" placeholder="Session End Date" value="@if($course){{ $course->end_date }}@else{{old('end_date')}}@endif" {{ !$course || $course && $course->is_end == 0 ? 'disabled="disabled"' : 'required="required"'}}>
                                        <span class="fa fa-calendar form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('end_date') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="name">Group Share Name<span class="text-danger">*</span></label>
                                        <input autofocus type="text" class="form-control" name="share_group_text" placeholder="Group Share Name" value="@if($course){{ $course->share_group_text }}@else{{old('share_group_text')}}@endif"  >
                                        <span class="fa fa-group form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('share_group_text') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="name">Group Share %<span class="text-danger">*</span></label>
                                        <input autofocus type="number" class="form-control" name="share_group_value" placeholder="Group Share %" value="@if($course){{ $course->share_group_value }}@else{{old('share_group_value')}}@endif">
                                        <span class="fa fa-percent form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('share_group_value') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="name">Registration Fee<span class="text-danger">*</span></label>
                                        <input autofocus type="number" name="registration_fee" class="form-control" placeholder="Registration Fee" value="@if($course){{ $course->registration_fee }}@else{{old('registration_fee')}}@endif" >
                                        <span class="fa fa-money form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('registration_fee') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="name">Admission Fee<span class="text-danger">*</span></label>
                                        <input autofocus type="number" class="form-control" name="admission_fee" placeholder="Admission Fee" value="@if($course){{ $course->admission_fee }}@else{{old('admission_fee')}}@endif" >
                                        <span class="fa fa-money form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('admission_fee') }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="name">Test Fee<span class="text-danger">*</span></label>
                                        <input autofocus type="number" name="test_fee" class="form-control" placeholder="Test Fee" value="@if($course){{ $course->test_fee }}@else{{old('test_fee')}}@endif" >
                                        <span class="fa fa-money form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('test_fee') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="name">Tuition Fee<span class="text-danger">*</span></label>
                                        <input autofocus type="number" class="form-control" name="tution_fee" placeholder="Tuition Fee" value="@if($course){{ $course->tution_fee }}@else{{old('tution_fee')}}@endif" >
                                        <span class="fa fa-money form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('tution_fee') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="name">Campus<span class="text-danger">*</span></label>
                                        <select class="form-control" name="campus_id" required>
                                            @foreach(\App\Campus::get() as $campus)
                                                <option value="{{$campus->id }}" {{ $course && $course->campus_id == $campus->id ? 'selected' : ''}}>{{ $campus->code.' | '.$campus->name }}</option>
                                            @endforeach
                                        </select>
                                        <span class="text-danger">{{ $errors->first('campus_id') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="name">Certificate<span class="text-danger">*</span></label>
                                        <select class="form-control" name="certificate" required>
                                            <option value="Not Required" {{ $course && $course->certificate == 'Not Required' ? 'selected' : ''}}>Not Required</option>
                                            <option value="Required" {{ $course && $course->certificate == 'Required' ? 'selected' : ''}}>Required</option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <a href="{{URL::route('course.index')}}" class="btn btn-default">Cancel</a>
                            <button type="submit" class="btn btn-info pull-right"><i class="fa @if($course) fa-refresh @else fa-plus-circle @endif"></i> @if($course) Update @else Add @endif</button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
<!-- END PAGE CONTENT-->

<!-- BEGIN PAGE JS-->
@section('extraScript')
    <script type="text/javascript">
        $(document).ready(function () {
            Generic.initCommonPageJS();
        });

        function disableDate(value){
            if(value == 0){
                $('input[name="end_date"]').prop('disabled', true).prop('required', false);
            }else{
                $('input[name="end_date"]').prop('disabled', false).prop('required', true);;
            }
        }
    </script>
@endsection
<!-- END PAGE JS-->
