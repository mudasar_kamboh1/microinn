<!-- Master page  -->
@extends('backend.layouts.master')

<!-- Page title -->
@section('pageTitle') Course Detail @endsection
<!-- End block -->

<!-- Page body extra css -->
@section('extraStyle')
    <style>
        @media print {
            @page {
                size:  A4 landscape;
                margin: 5px;
            }
        }
    </style>
@endsection
<!-- End block -->

<!-- BEGIN PAGE CONTENT-->
@section('pageContent')
    <!-- Section header -->
    <section class="content-header">
        <div class="btn-group">
            <a href="#"  class="btn-ta btn-sm-ta btn-print btnPrintInformation"><i class="fa fa-print"></i> Print</a>
        </div>
        <div class="btn-group">
            <a href="{{URL::route('course.edit',$course->id)}}" class="btn-ta btn-sm-ta"><i class="fa fa-edit"></i> Edit</a>
        </div>

        <ol class="breadcrumb">
            <li><a href="{{URL::route('user.dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{URL::route('course.index')}}"><i class="fa icon-course"></i> Course</a></li>
            <li class="active">View</li>
        </ol>
    </section>
    <!-- ./Section header -->
    <!-- Main content -->
    <section class="content main-contents">
        <div class="row">
            <div class="col-md-12">
                <div id="printableArea">
                    <div class="box box-info">
                    <div class="box-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="">Name</label>
                                </div>
                                <div class="col-md-4">
                                    <p for=""> {{$course->name}}</p>
                                </div>
                                <div class="col-md-2">
                                    <label for="">Detail</label>
                                </div>
                                <div class="col-md-4">
                                    <p for=""> {{$course->detail}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="">Institute</label>
                                </div>
                                <div class="col-md-4">
                                    <p for=""> {{$course->institute ? $course->institute->institute : ''}}</p>
                                </div>
                                <div class="col-md-2">
                                    <label for="">Campus</label>
                                </div>
                                <div class="col-md-4">
                                    <p for=""> {{$course->campus ? $course->campus->code.' '.$course->campus->name : ''}}</p>
                                </div>
                            </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label for="">Duration</label>
                            </div>
                            <div class="col-md-4">
                                <p for=""> {{$course->duration}} Days</p>
                            </div>
                            <div class="col-md-2">
                                <label for="">End Date Apply</label>
                            </div>
                            <div class="col-md-4">
                                <p for=""> {{$course->is_end == 0 ? 'No' : \Carbon\Carbon::parse($course->end_date)->format('d-m-Y')}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label for="">Group Share Name</label>
                            </div>
                            <div class="col-md-4">
                                <p for=""> {{$course->share_group_text}}</p>
                            </div>
                            <div class="col-md-2">
                                <label for="">Group Share %</label>
                            </div>
                            <div class="col-md-4">
                                <p for=""> {{$course->share_group_value}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label for="">Registration Fee</label>
                            </div>
                            <div class="col-md-4">
                                <p for=""> {{$course->registration_fee}}</p>
                            </div>
                            <div class="col-md-2">
                                <label for="">Admission Fee</label>
                            </div>
                            <div class="col-md-4">
                                <p for=""> {{$course->admission_fee}}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-2">
                                <label for="">Test Fee</label>
                            </div>
                            <div class="col-md-4">
                                <p for=""> {{$course->test_fee}}</p>
                            </div>
                            <div class="col-md-2">
                                <label for="">Tuition Fee</label>
                            </div>
                            <div class="col-md-4">
                                <p for=""> {{$course->tution_fee}}</p>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- /.content -->
@endsection
<!-- END PAGE CONTENT-->

<!-- BEGIN PAGE JS-->
@section('extraScript')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.btnPrintInformation').click(function () {
                $('ul.nav-tabs li:not(.active)').addClass('no-print');
                $('ul.nav-tabs li.active').removeClass('no-print');
                window.print();
            });
        });
    </script>
@endsection
<!-- END PAGE JS-->
