@extends('frontend.layouts.master')
@section('pageTitle') @lang('site.menu_home') @endsection
@section('extraStyle')
    <link type="text/css" rel="stylesheet" href="{{ asset('/frontend/rs-plugin/css/settings.css') }}" />
@endsection

@section('pageContent')
    <div class="inner-banner">
        <div class="opacity">
            <div class="container">
                <h2>Sign Up</h2>
                <ul>

                </ul>
            </div> <!-- /.container -->
        </div> <!-- /.opacity -->
    </div> <!-- /.inner-banner -->



    <!-- Sign-Up Modal -->
    <div class="signUpModal theme-modal-box">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    {{--<h3>Login with Social Networks</h3>
                    <ul class="clearfix">
                        <li class="float-left"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i> facebook</a></li>
                        <li class="float-left"><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i> Google</a></li>
                        <li class="float-left"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i> Twitter</a></li>
                        <li class="float-left"><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i> Linkedin</a></li>
                    </ul>--}}
                    <form action="{{url('/storeSignup')}}" method="post" id="eligible-form" class="form-validation">
                        {{--                        <h3>Sign Up</h3>--}}
                        <meta name="_token" content="{!! csrf_token() !!}" />

                        <div class="wrapper">
                            <input type="text" placeholder=Name" id="signup-name" name="name">
                            <input type="text" placeholder="Username" id="signup-user_name" name="username">
                            <input type="email" placeholder="Email" id="signup-email" name="email">
                            <input type="text" placeholder="Phone no" id="signup-phone" name="phone_no">
                            <input type="password" placeholder="Password" id="signup-password" name="password">
                            <input type="password" placeholder="Repeat Password" id="" name="reset_password">
{{--                            <button class="p-bg-color hvr-trim" type="submit">Sing up</button>--}}
                            <input type="submit" value="Next" id="eligibilty" class="tran3s p-bg-color">

                        </div>
                    </form>
                </div> <!-- /.modal-body -->
            </div> <!-- /.modal-content -->
        </div> <!-- /.modal-dialog -->
    </div> <!-- /.signUpModal -->


@endsection
