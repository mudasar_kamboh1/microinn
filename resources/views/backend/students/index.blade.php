<!-- Master page  -->
@extends('backend.layouts.master')

{{--<!-- Page title -->--}}
@section('pageTitle') Events @endsection
{{--<!-- End block -->--}}

{{--<!-- Page body extra class -->--}}
@section('bodyCssClass') @endsection
{{--<!-- End block -->--}}

{{--<!-- BEGIN PAGE CONTENT-->--}}
@section('pageContent')
    {{--    <!-- Section header -->--}}
    <section class="content-header">
        <h1>
            Students
            <small>List</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{URL::route('site.dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#"><i class="fa icon-teacher"></i> Students</a></li>

        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title">Jobs</h3>

                        <div class="box-tools pull-right">
                            <a class="btn btn-info btn-sm" href="{{ URL::route('students.create') }}"><i class="fa fa-plus-circle"></i> Add New</a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="eventList" class="table table-bordered table-striped list_view_table">
                            <thead>
                            <tr>
                                <th width="40%">Image</th>
                                <th width="20%">Name</th>
                                <th width="30%">Description</th>
                                <th width="10%">Action</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($students as $student)
                                <tr>

                                    <td>
                                        <img class="img-responsive" style="max-height: 200px;" src="@if($student->image){{ asset('storage/students')}}/{{ $student->image }}@else{{asset('images/default/student.png')}}@endif" alt="">

                                    </td>
                                    <td> {{ $student->name }}</td>
                                    <td> {{ $student->description }}</td>
                                    <td>
                                        <div class="btn-group">
                                            <a title="Edit" href="{{URL::route('students.edit',$student->id)}}" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></a>
                                        </div>

                                        <div class="btn-group">
                                            <form class="myAction" method="POST" action="{{URL::route('students.destroy',$student->id)}}">
                                                {{ method_field('DELETE') }}
                                                @csrf
                                                <button type="submit" class="btn btn-danger btn-sm" title="Delete">
                                                    <i class="fa fa-fw fa-trash"></i>
                                                </button>
                                            </form>
                                        </div>
                                    </td>
                                    <td> </td>
                                </tr>
                            @endforeach

                            </tbody>
                            <tfoot>
                            <tr>
                                <th width="40%">Image</th>
                                <th width="20%">Name</th>
                                <th width="30%">Description</th>
                                <th width="10%">Action</th>
                            </tr>
                            </tfoot>
                        </table>
                        {{ $students->links() }}
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>

    </section>
    <!-- /.content -->
@endsection
<!-- END PAGE CONTENT-->

<!-- BEGIN PAGE JS-->
@section('extraScript')
    <script type="text/javascript">
        $(document).ready(function () {
            Site.EventInit();
            initDeleteDialog();
        });
    </script>
@endsection
<!-- END PAGE JS-->
