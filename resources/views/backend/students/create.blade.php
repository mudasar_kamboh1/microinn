<!-- Master page  -->
@extends('backend.layouts.master')

<!-- Page title -->
@section('pageTitle') Jobs @endsection
<!-- End block -->

<!-- Page body extra class -->
@section('bodyCssClass') @endsection
<!-- End block -->

<!-- BEGIN PAGE CONTENT-->
@section('pageContent')
    <!-- Section header -->
    <section class="content-header">
        <h1>
            Jobs
            {{--            <small>@if($teacher) Update @else Add New @endif</small>--}}
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{URL::route('user.dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{URL::route('jobs.index')}}"><i class="fa icon-teacher"></i> Jobs</a></li>
            {{--            <li class="active">@if($teacher) Update @else Add @endif</li>--}}
        </ol>
    </section>
    <!-- ./Section header -->
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <form novalidate id="entryForm" action="@if($students){{URL::Route('students.update', $students->id)}} @else {{URL::Route('students.store')}}  @endif" method="post" enctype="multipart/form-data">
                        <div class="box-header">
                            <h3 class="box-title">@if($students) Update @else Add @endif Students<span class="text-danger"> * Marks are required feild</span></h3>
                        </div>
                        <div class="box-body">
                            @csrf
                            @if($students)  {{ method_field('PATCH') }} @endif
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="name">Name<span class="text-danger">*</span></label>
                                        <input autofocus type="text" class="form-control" name="name" placeholder="name" value="@if($students){{ $students->name }}@else{{ old('name') }} @endif" required minlength="2" maxlength="255">
                                        <span class="fa fa-info form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                <div class="form-group has-feedback">
                                    <label for="cover_photo">Cover Photo</label>
                                    <input  type="file" class="form-control" accept=".jpeg, .jpg, .png" name="image"  >
                                    <span class="glyphicon glyphicon-open-file form-control-feedback"></span>
                                    <span class="text-danger">{{ $errors->first('image') }}</span>
                                </div>
                            </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group has-feedback">
                                    <label for="address">Description</label>
                                    <textarea name="description" class="form-control" required maxlength="500" >@if($students){{ $students->description }}@else{{ old('description') }} @endif</textarea>
                                    {{--<span class="fa fa-location-arrow form-control-feedback"></span>--}}
                                    <span class="text-danger">{{ $errors->first('description') }}</span>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="box-footer">
                            <a href="{{URL::route('students.index')}}" class="btn btn-default">Cancel</a>
                            <button type="submit" class="btn btn-info pull-right"><i class="fa fa-plus-circle"></i>  Add </button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
<!-- END PAGE CONTENT-->

<!-- BEGIN PAGE JS-->
@section('extraScript')
    <script type="text/javascript">
        $(document).ready(function () {
            Generic.initCommonPageJS();
        });
    </script>
@endsection
