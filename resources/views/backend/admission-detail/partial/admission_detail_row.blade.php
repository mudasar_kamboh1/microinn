<!-- /.box-header -->
<div class="box-body updated_row">
    <table id="eventList" class="table table-bordered table-striped list_view_table">
        <thead>
        <tr>
            <th>Name</th>
            <th>phone No</th>
            <th>Email</th>
            <th>Qualification</th>
            <th>Visited Page</th>
        </tr>
        </thead>
        <tbody>
        @foreach($admission as $admissionRequest)
            <tr>

                <td>{{ $admissionRequest->name }}</td>
                <td> {{ $admissionRequest->phone_no }}</td>
                <td> {{ $admissionRequest->email }}</td>
                <td> {{ $admissionRequest->qualification }}</td>
                <td>{{ \Config::get('constant.route_detail.'.str_replace('-','_',$admissionRequest->type)) }}</td>
            </tr>
        @endforeach

        </tbody>
    </table>
</div>
<!-- /.box-body -->
