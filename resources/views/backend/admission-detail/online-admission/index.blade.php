<!-- Master page  -->
@extends('backend.layouts.master')

{{--<!-- Page title -->--}}
@section('pageTitle') Form Request @endsection
{{--<!-- End block -->--}}

{{--<!-- Page body extra class -->--}}
@section('bodyCssClass') @endsection
{{--<!-- End block -->--}}

{{--<!-- BEGIN PAGE CONTENT-->--}}
@section('pageContent')
    <style>
        td {
            text-transform: capitalize;
        }
    </style>
    {{--    <!-- Section header -->--}}
    <section class="content-header">
        <h1>
            Student Admission Inquiry
            <small>List</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{URL::route('site.dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#"><i class="fa icon-teacher"></i> Student Admission Inquiry</a></li>

        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">

                    <!-- /.box-header -->
                    <div class="box-body updated_row">
                        <table id="eventList" class="table table-bordered table-striped list_view_table">
                            <thead>
                            <tr>
                                <th>inquiry_no</th>
                                <th>inquiry_date</th>
                                <th>name</th>
                                <th>gender</th>
                                <th>contact</th>
                                <th>action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($admission as $admissionRequest)
                                <tr>

                                    <td>{{ $admissionRequest->inquiry_no }}</td>
                                    <td> {{ $admissionRequest->inquiry_date }}</td>
                                    <td> {{ $admissionRequest->name }}</td>
                                    <td> {{ $admissionRequest->gender==0?'female':'Male' }}</td>
                                    <td> {{ $admissionRequest->contact }}</td>
                                    <td> <div class="btn-group" style="float: left; margin-left: 10px;">
                                            <a title="Detail" href="{{URL::route('online-admission-detail',$admissionRequest->id)}}" class="btn btn-info btn-sm"><i class="fa fa-address-book-o"></i></a>

                                        </div></td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>

    </section>
    <!-- /.content -->
@endsection
<!-- END PAGE CONTENT-->

<!-- BEGIN PAGE JS-->
@section('extraScript')
    <script type="text/javascript">
        $(document).ready(function () {
            Site.EventInit();
            initDeleteDialog();
        });
    </script>
@endsection
<!-- END PAGE JS-->
