<!-- Master page  -->
@extends('backend.layouts.master')

<!-- Page title -->
@section('pageTitle') Events @endsection
<!-- End block -->

<!-- BEGIN PAGE CSS-->
@section('extraStyle')
@endsection

<!-- Page body extra class -->
@section('bodyCssClass') @endsection
<!-- End block -->

<!-- BEGIN PAGE CONTENT-->
@section('pageContent')
    <!-- Section header -->
    <section class="content-header">
        <h1>
            Admission Inquiry Detail
            <small>List</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{URL::route('site.dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{URL::route('job-requests.index')}}"><i class="fa icon-Event"></i> Job Request</a></li>
            <li class="active">Admission Inquiry Detail</li>
        </ol>
    </section>
    <!-- ./Section header -->
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group has-feedback">
                            <label for="title">Inquiry No:</label><br/>
                            <span class="text-info">{{ $admission->inquiry_no }}</span>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="title">Inquiry Date:</label><br/>
                            <span class="text-info">{{ $admission->inquiry_date }}</span>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="title">Name:</label><br/>
                            <span class="text-info">{{ $admission->name }}</span>

                        </div>
                        <div class="form-group has-feedback">
                            <label for="title">Gender:</label><br/>
                            <span class="text-info">{{ $admission->gender=='0'?'Female':'Male' }}</span>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="event_time">Contact:</label><br/>
                            <span class="text-info">{{ $admission->contact}}</span>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="title">Town:</label><br/>
                            <span class="text-info">{{ $admission->town}}</span>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="title">Qualification:</label><br/>
                            <span class="text-info">{{ $admission->qualification}}</span>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="title">Total Marks:</label><br/>
                            <span class="text-info">{{ $admission->total_marks }}</span>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="title">Institute:</label><br/>
                            <span class="text-info">{{ $institute->institute }}</span>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="title">Degree:</label><br/>
                            <span class="text-info">{{ $degree->title }}</span>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="title">Study:</label><br/>
                            <span class="text-info">{{ $study->title }}</span>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="title">Availability:</label><br/>
                            <span class="text-info">{{ $admission->availability }}</span>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="title">Info Source:</label><br/>
                            <span class="text-info">{{ $admission->info_source }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
<!-- END PAGE CONTENT-->
<!-- BEGIN PAGE JS-->
@section('extraScript')
    <!-- editor js -->
    <script>
        //editor jQuery Patch fixing
        jQuery = $;
    </script>
    <script src="{{ asset('/js/editor.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            Site.EventInit();
        });
    </script>
@endsection
<!-- END PAGE JS-->
