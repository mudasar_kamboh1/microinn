<!-- Master page  -->
@extends('backend.layouts.master')

{{--<!-- Page title -->--}}
@section('pageTitle') Form Request @endsection
{{--<!-- End block -->--}}

{{--<!-- Page body extra class -->--}}
@section('bodyCssClass') @endsection
{{--<!-- End block -->--}}

{{--<!-- BEGIN PAGE CONTENT-->--}}
@section('pageContent')
    <style>
        td {
            text-transform: capitalize;
        }
    </style>
    {{--    <!-- Section header -->--}}
    <section class="content-header">
        <h1>
            Admission Detail
            <small>List</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{URL::route('site.dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#"><i class="fa icon-teacher"></i> Jobs</a></li>

        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="col-md-3">
                        <br>
                    <select name="institute" id="status_selector" class="form-control">
                        <option value="">Select</option>
                        @foreach( \Config::get('constant.route_detail') as $k => $route)
                            <option value="{{$k}}">{{ $route }}</option>
                        @endforeach
                    </select>
                        <br>
                    </div>
                @include('backend.admission-detail.partial.admission_detail_row')
                </div>
            </div>
        </div>

    </section>
    <!-- /.content -->
@endsection
<!-- END PAGE CONTENT-->

<!-- BEGIN PAGE JS-->
@section('extraScript')
    <script type="text/javascript">
        $(document).ready(function () {
            Site.EventInit();
            initDeleteDialog();
        });
    </script>
    <script>
        $('#status_selector').change(function () {
            var selectedInstitute = $(this).children("option:selected").val();
            $.ajax({
                url: '/admission-detail/' + selectedInstitute ,
                method: 'GET',
                type: 'JSON',
                success: function (response) {
                    $(".updated_row").replaceWith(response.view);

                }
            })
        });
    </script>
@endsection
<!-- END PAGE JS-->
